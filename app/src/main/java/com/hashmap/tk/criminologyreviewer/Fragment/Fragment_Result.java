package com.hashmap.tk.criminologyreviewer.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.hashmap.tk.criminologyreviewer.Adapter.Adapter_Fragment_Results;
import com.hashmap.tk.criminologyreviewer.Database.Datasource;
import com.hashmap.tk.criminologyreviewer.Database.MySQLiteHelper;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class Fragment_Result extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    static String categoryId = null, categoryTitle = null;
    ProgressDialog mProgressDialog;
    View mainView, layoutBarGraph, layoutList;
    Button btnShowList, btnShowGraph;
    ListView lvListOfCategories, lvListOfResults;
    List<Category> arraylist, resultList;
    LinearLayout layoutTab;
    Adapter_Fragment_Results adapterFragmentResults;
    Datasource dbsource;
    MySQLiteHelper mySQLiteHelper;
    boolean mWordWrapEnabled = false;
    SharedPref sharedPref;
    private LineChart mChart;
    private SwingRightInAnimationAdapter mAnimAdapter;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_result, menu);
        super.onCreateOptionsMenu(menu, inflater);
        sharedPref = new SharedPref(getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                new DeleteData().execute();
                break;

        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_result, container, false);
        dbsource = new Datasource(getActivity());
        mySQLiteHelper = new MySQLiteHelper(getActivity());
        dbsource.open();

        initVars();
        setupChart();
        setCategoryList();

        lvListOfCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getActivity(), arraylist.get(i).getTitle() + " " + arraylist.get(i).getId(), Toast.LENGTH_SHORT).show();
                setHasOptionsMenu(true);
                setCategoryList();
                categoryId = arraylist.get(i).getId();
                categoryTitle = arraylist.get(i).getTitle();
                Toast.makeText(getActivity(), "Retreiving results", Toast.LENGTH_SHORT).show();
                new ParseDataTask().execute();

            }
        });

        return mainView;
    }

    private void setCategoryList() {
        btnShowGraph.setVisibility(View.GONE);
        layoutTab.setVisibility(View.GONE);
        layoutBarGraph.setVisibility(View.INVISIBLE);
        layoutList.setVisibility(View.VISIBLE);

        //Viewing results in list
        arraylist = new ArrayList<>();
        arraylist = dbsource.queryCategory();
        adapterFragmentResults = new Adapter_Fragment_Results(getActivity(), arraylist, "category");

        if (!(mAnimAdapter instanceof SwingRightInAnimationAdapter)) {
            mAnimAdapter = new SwingRightInAnimationAdapter(adapterFragmentResults);
            mAnimAdapter.setAbsListView(lvListOfCategories);
            lvListOfCategories.setAdapter(mAnimAdapter);
            mAnimAdapter.notifyDataSetChanged();

        }
    }

    private void initVars() {
        mChart = (LineChart) mainView.findViewById(R.id.chart1);

        layoutTab = (LinearLayout) mainView.findViewById(R.id.layoutTab);
        layoutList = mainView.findViewById(R.id.layoutList);
        layoutBarGraph = mainView.findViewById(R.id.layoutBarGraph);

        lvListOfCategories = (ListView) layoutList.findViewById(R.id.lvResults);
        lvListOfResults = (ListView) layoutBarGraph.findViewById(R.id.lvListOfResults);

        btnShowList = (Button) mainView.findViewById(R.id.btnShowList);
        btnShowGraph = (Button) mainView.findViewById(R.id.btnShowGraph);
        btnShowList.setOnClickListener(this);
        btnShowGraph.setOnClickListener(this);

    }

    private void setupChart() {

        // no description text
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart.");

        // enable touch gestures
        mChart.setTouchEnabled(true);

        mChart.setDragDecelerationFrictionCoef(0.9f);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        mChart.setBackgroundColor(Color.LTGRAY);
        mChart.animateX(2500);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.WHITE);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextSize(12f);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setSpaceBetweenLabels(1);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        mChart.animateY(2500);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnShowList:
                setCategoryList();
                setHasOptionsMenu(false);
                break;

            case R.id.btnShowGraph:
                layoutBarGraph.setVisibility(View.VISIBLE);
                layoutList.setVisibility(View.INVISIBLE);
                mChart.animateY(3000);

                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


    }

    private void setChartData() {

        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < resultList.size(); i++) {
            xVals.add("Take " + (i + 1));
        }

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        for (int i = 0; i < resultList.size(); i++) {

            yVals1.add(new Entry(Float.parseFloat(resultList.get(i).getScore()), i));
        }

        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals1, categoryTitle);
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1.setColor(ColorTemplate.getHoloBlue());
        set1.setCircleColor(Color.WHITE);
        set1.setLineWidth(2f);
        set1.setCircleSize(3f);
        set1.setFillAlpha(65);
        set1.setFillColor(ColorTemplate.getHoloBlue());
        set1.setHighLightColor(Color.rgb(244, 117, 117));
        set1.setDrawCircleHole(false);

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);
        data.setValueTextColor(Color.WHITE);
        data.setValueTextSize(9f);

        // set data
        mChart.setData(data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    // Getting data from Parse
    private class ParseDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("Retrieving data sets percentage...");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            SimpleDateFormat formatter = new SimpleDateFormat("MMM.dd.yy");
            resultList = new ArrayList<Category>();

            try {
                ParseUser currentUser = ParseUser.getCurrentUser();
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("ScoreBoard");
                query.whereEqualTo("owner", sharedPref.getKeyObjectid());
                query.whereEqualTo("category_id", categoryId);
                query.orderByAscending("createdAt");
                List<ParseObject> ob = query.find();

                for (ParseObject parseObject : ob) {
                    Category category = new Category();
                    category.setScore((String) parseObject.get("highscore"));
                    category.setDate(String.valueOf(formatter.format(parseObject.getCreatedAt())));
                    // category.setTitle(dbsource.getCategoryTitle((String) parseObject.get("category_id")));


                    resultList.add(category);
                    Log.i("resultList", category.getScore() + " " + category.getTitle() /* + formatter.format(*//*parseObject.get("createdAt")*/);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // Set data for line graphs
            setChartData();
            layoutBarGraph.setVisibility(View.VISIBLE);
            layoutList.setVisibility(View.INVISIBLE);
            layoutTab.setVisibility(View.VISIBLE);
            mChart.animateY(3000);

            // Set data for ListView
            adapterFragmentResults = new Adapter_Fragment_Results(getActivity(), resultList, "categories");
            mAnimAdapter = new SwingRightInAnimationAdapter(adapterFragmentResults);
            mAnimAdapter.setAbsListView(lvListOfResults);
            lvListOfResults.setAdapter(mAnimAdapter);

            mProgressDialog.dismiss();


        }
    }

    private class DeleteData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("Delete Result");
            mProgressDialog.setMessage("Deleting...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            SimpleDateFormat formatter = new SimpleDateFormat("MMM.dd.yy");
            resultList = new ArrayList<Category>();

            try {
                ParseUser currentUser = ParseUser.getCurrentUser();
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("ScoreBoard");
                query.whereEqualTo("owner", sharedPref.getKeyObjectid());
                query.whereEqualTo("category_id", categoryId);
                List<ParseObject> ob = query.find();

                for (ParseObject parseObject : ob) {
                    parseObject.deleteInBackground();
                }
            } catch (com.parse.ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();
            new ParseDataTask().execute();
        }
    }
}
