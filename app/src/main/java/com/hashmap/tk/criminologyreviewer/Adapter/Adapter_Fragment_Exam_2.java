package com.hashmap.tk.criminologyreviewer.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hashmap.tk.criminologyreviewer.Activity.Activity_Exam;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.R;

import java.util.List;


public class Adapter_Fragment_Exam_2 extends BaseAdapter {

    private final List<Category> categoryList;
    Context context;
    String _indicator;

    public Adapter_Fragment_Exam_2(Context context, List<Category> items, String indicator) {
        categoryList = items;
        this.context = context;
        this._indicator = indicator;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.valueOf(categoryList.get(position).getId());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder holder = null;
        convertView = null;
        String time;
        if (convertView == null) {

            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_item, parent, false);
            holder = new ViewHolder();

            holder.txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            holder.txtSet = (TextView) view.findViewById(R.id.txtSet);
            holder.txtPercent = (TextView) view.findViewById(R.id.txtPercent);
            holder.txtLimit = (TextView) view.findViewById(R.id.txtTimeLimit);

            holder.txtTitle.setText(categoryList.get(position).getTitle());
            holder.txtSet.setText(categoryList.get(position).getSet());
            if (categoryList.get(position).getHighscore() != null) {
                holder.txtPercent.setText(String.valueOf(categoryList.get(position).getHighscore() + "%"));
                if (categoryList.get(position).getTimelimit().equals("60")) {
                    time = "1 hr.";
                } else if (categoryList.get(position).getTimelimit().equals("0")) {
                    time = "Not set";
                } else {
                    time = categoryList.get(position).getTimelimit() + " mins.";
                }
                holder.txtLimit.setText(time);
            }
            try {
//                Log.i("Recycler", categoryList.get(position).getHighscore());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i("Recycler", categoryList.get(position).getTitle());
                Intent intent = new Intent(context.getApplicationContext(), Activity_Exam.class);
                intent.putExtra("categoryID", position);
                context.startActivity(intent);

            }
        });
//        notifyDataSetChanged();
        return view;
    }


    class ViewHolder {
        public View mView;
        public TextView txtTitle;
        public TextView txtSet;
        public TextView txtPercent;
        public TextView txtLimit;
    }
}
