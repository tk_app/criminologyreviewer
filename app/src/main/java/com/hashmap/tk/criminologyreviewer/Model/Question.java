package com.hashmap.tk.criminologyreviewer.Model;

import android.database.Cursor;

import com.hashmap.tk.criminologyreviewer.Utils.Util;

public class Question {
    public long id;
    public int category_id;
    public String question;
    public String[] choices;
    public int answer;
    public int yourAnswer;
    public static String time;
    public static String answerWord;
    public static String yourAnswerWord;

    public static String getTime() {
        return time;
    }

    public static void setTime(String time) {
        Question.time = time;
    }

    public static String getAnswerWord() {
        return answerWord;
    }

    public static void setAnswerWord(String answerWord) {
        Question.answerWord = answerWord;
    }

    public static String getYourAnswerWord() {
        return yourAnswerWord;
    }

    public static void setYourAnswerWord(String yourAnswerWord) {
        Question.yourAnswerWord = yourAnswerWord;
    }

    public int getYourAnswer() {
        return yourAnswer;
    }

    public void setYourAnswer(int yourAnswer) {
        this.yourAnswer = yourAnswer;
    }

    public Question(Cursor c) {
        question = c.getString(2);
        choices = (String[]) Util.deserialize(c.getBlob(3));
        answer = c.getInt(4);
        id = c.getLong(0);
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String[] getChoices() {
        return choices;
    }

    public void setChoices(String[] choices) {
        this.choices = choices;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
