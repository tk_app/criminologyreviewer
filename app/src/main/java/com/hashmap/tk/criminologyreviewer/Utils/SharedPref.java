package com.hashmap.tk.criminologyreviewer.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPref {
    // (make variables public to access from outside)
    public static final String KEY_TOTAL_QUESTION = "TotalQuestion";
    public static final String KEY_TIME_LIMIT = "TimeLimit";//20mins-0,30mins-1,1hr-2,Untimed-3
    public static final String KEY_SOUND = "Sound";
    public static final String KEY_AUTO_NEXT = "AutoNext";
    public static final String KEY_TIMESUP = "TimesUp";
    public static final String KEY_USERNAME = "Username";
    public static final String KEY_NAME = "Name";
    public static final String KEY_OBJECTID = "ObjectID";
    public static final String KEY_EMAIL = "Email";
    public static final String KEY_PASSWORD = "Password";
    public static final String KEY_TYPE = "AccessType";
    // Sharedpref file name
    private final String PREF_NAME = "com.hashmap.tk.criminologyreviewer_preferences";
    public String KEY_INITIALIZATION = "Initalize";
    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    public SharedPref(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean getInitialization() {
        return pref.getBoolean(KEY_INITIALIZATION, false);
    }

    public void setInitialization(boolean init) {
        editor.putBoolean(KEY_INITIALIZATION, init);
        editor.commit();
    }

    public int getTotalQuestion() {
        return pref.getInt(KEY_TOTAL_QUESTION, 0);
    }

    public void setTotalQuestion(int number) {
//       pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
//       editor = pref.edit();
        editor.putInt(KEY_TOTAL_QUESTION, number);
        editor.commit();
    }

    public String getTimeLimit() {
        return pref.getString(KEY_TIME_LIMIT, "0");
    }

    public void setTimeLimit(String limit) {
        editor.putString("TimeLimit", limit);
        editor.commit();
    }

    public void setTimesUp(String limit) {
        editor.putString(KEY_TIMESUP, limit);
        editor.commit();
    }

    public String getTimeTimesUp() {
        return pref.getString(KEY_TIMESUP, "0");
    }

    public boolean getSound() {
        return pref.getBoolean(KEY_SOUND, false);
    }

    public void setSound(boolean isOn) {
        editor.putBoolean(KEY_SOUND, isOn);
        editor.commit();
    }

    public boolean isOn() {
        return pref.getBoolean(KEY_SOUND, false);
    }

    public boolean getAutoNext() {
        return pref.getBoolean(KEY_AUTO_NEXT, false);
    }

    public boolean isAutoNext() {
        return pref.getBoolean(KEY_AUTO_NEXT, false);
    }

    public void setAutoNext(boolean isOn) {
        editor.putBoolean(KEY_AUTO_NEXT, isOn);
        editor.commit();
    }

    public String getKeyUsername() {
        return pref.getString(KEY_USERNAME, "");
    }

    public void setKeyUsername(String username) {
        editor.putString(KEY_USERNAME, username);
        editor.commit();
    }

    public String getKeyPassword() {
        return pref.getString(KEY_PASSWORD, "");
    }

    public void setKeyPassword(String password) {
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }

    public String getKeyType() {
        return pref.getString(KEY_TYPE, "");

    }

    public void setKeyType(String type) {
        editor.putString(KEY_TYPE, type);
        editor.commit();
    }




    public String getKeyName() {
        return pref.getString(KEY_NAME, "");

    }

    public void setKeyName(String name) {
        editor.putString(KEY_NAME, name);
        editor.commit();
    }



    public String getKeyEmail() {
        return pref.getString(KEY_EMAIL, "");

    }

    public void setKeyEmail(String type) {
        editor.putString(KEY_EMAIL, type);
        editor.commit();
    }

    public String getKeyObjectid() {
        return pref.getString(KEY_OBJECTID, "");

    }

    public void setKeyObjectid(String type) {
        editor.putString(KEY_OBJECTID, type);
        editor.commit();
    }
}
