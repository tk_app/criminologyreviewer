package com.hashmap.tk.criminologyreviewer.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Adapter.SectionsPagerAdapter;
import com.hashmap.tk.criminologyreviewer.Database.Datasource;
import com.hashmap.tk.criminologyreviewer.Fragment.PlaceholderFragment;
import com.hashmap.tk.criminologyreviewer.Fragment.PlaceholderFragment.OnFragmentInteractionListener;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.Model.Question;
import com.hashmap.tk.criminologyreviewer.Model.Score;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Activity_Exam extends AppCompatActivity implements OnFragmentInteractionListener {

    Datasource datasource;
    Button btnNext, btnBack;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private int mCategoryId;
    private Question[] mQuestions;
    private int currentPage;
    private ProgressDialog dialog;
    public static String timesUp = null, timeLimit = null;

    CountDownTimer countDownTimer;
    static boolean isTimerRunning = false;
    public static TextView txtTime;
    String output;
    long seconds;
    int millis;
    public static TextView txtCurrent, txtCount, txtCorrect, txtWrong;
    public static int intCorrect = 0;
    public static int intWrong = 0;
    private static int totalQuestion = 0, intAnsweredQuestion = 0;
    public static List<Fragment> fragmentList;
    SharedPref sharedPref;
    private String timesup;
    private List<Question> questionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        sharedPref = new SharedPref(this);
        datasource = new Datasource(Activity_Exam.this);
        datasource.open();
        Intent in = getIntent();
        mCategoryId = in.getIntExtra("categoryID", 0);
        Category.setCategoryId(mCategoryId);

        btnNext = (Button) findViewById(R.id.btnNext);
        btnBack = (Button) findViewById(R.id.btnBack);
        mViewPager = (ViewPager) findViewById(R.id.container);
        txtCurrent = (TextView) findViewById(R.id.txtCurrent);
        txtCount = (TextView) findViewById(R.id.txtCount);
        txtCorrect = (TextView) findViewById(R.id.txtCorrect);
        txtWrong = (TextView) findViewById(R.id.txtWrong);
        txtTime = (TextView) findViewById(R.id.txtTime);

        // Setting up countdowntimer
        examTimer();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                fragmentList = getAllFragment();
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragmentList);
                // Set up the ViewPager with the sections adapter.
                return null;
            }

            @Override
            protected void onPreExecute() {
                dialog = new ProgressDialog(Activity_Exam.this);
                dialog.setIndeterminate(true);
                dialog.setMessage("Loading question please wait...");
                dialog.setCancelable(false);
                dialog.show();
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                countDownTimer.start();
//                checkTimer();
                txtCount.setText(String.valueOf(selectQuestions().size()));
                mViewPager.setAdapter(mSectionsPagerAdapter);
                mViewPager.setOffscreenPageLimit(fragmentList.size());
                dialog.dismiss();
                super.onPostExecute(aVoid);
            }
        }.execute();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                txtCurrent.setText(String.valueOf(currentPage + 1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                examTimeStatement();
//                PlaceholderFragment pf = (PlaceholderFragment)mSectionsPagerAdapter.getItem(currentPage-1);
//                pf.disableButton(true);
            }
        });


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(currentPage - (1), true);
            }
        });
    }

    private void examTimeStatement() {
        totalQuestion = sharedPref.getTotalQuestion();
        intAnsweredQuestion = Score.getAnswered();
//        Log.i("Counter: ", "TotQuest: " + totalQuestion + " AnsQuest: " + intAnsweredQuestion);

        if (sharedPref.getTimeLimit().equals("0")) {
            // For Untimed Exam
            if (totalQuestion != intAnsweredQuestion) {
                mViewPager.setCurrentItem(currentPage + (1), true);
            } else {
                Toast.makeText(Activity_Exam.this, "Stop Untimed!", Toast.LENGTH_SHORT).show();
                examFinished();
            }
        } else {
            // For Timed Exam
            if (totalQuestion != intAnsweredQuestion && !txtTime.getText().equals("00:00:00")) {
                mViewPager.setCurrentItem(currentPage + (1), true);
            } else {
                Toast.makeText(Activity_Exam.this, "Stop Timed!", Toast.LENGTH_SHORT).show();
                examFinished();
            }
        }
    }

    private void examFinished() {

        if (sharedPref.getTimeLimit().equals("0")) {
            timesUp = "Not Set";
            timeLimit = sharedPref.getTimeLimit();
        } else {
            timeLimit = sharedPref.getTimeLimit();
        }
        Intent intent = new Intent(this, Activity_ScoreBoard.class);
        intent.putExtra("totalquestion", totalQuestion);
        intent.putExtra("timelimit", timeLimit);
        intent.putExtra("totalanswered", Score.getAnswered());
        intent.putExtra("correct", Score.getCorrect());
        intent.putExtra("wrong", Score.getWrong());
        intent.putExtra("timesup", Question.getTime());
        intent.putExtra("category_id", String.valueOf(Category.getCategoryId()));
        startActivity(intent);
        intCorrect = 0;
        intWrong = 0;
        Score.setAnswered(0);
        Score.setCorrect(0);
        Score.setWrong(0);
//        checkTimer();
        countDownTimer.cancel();
        this.finish();
    }

    private void checkTimer() {
        if (!isTimerRunning) {
            countDownTimer.start();
            isTimerRunning = true;
        } else {
            countDownTimer.cancel();
            isTimerRunning = false;
        }
    }


    private void examTimer() {
        if (sharedPref.getAutoNext()) {
            btnBack.setVisibility(View.INVISIBLE);
            btnNext.setVisibility(View.INVISIBLE);
        }
        countDownTimer = new CountDownTimer(Integer.parseInt(sharedPref.getTimeLimit()), 1000) {
            @Override
            public void onTick(long l) {
                millis = Integer.parseInt(String.valueOf(l));
//                Log.i("LONG: ", String.valueOf(l));
                txtTime.setText(formatTimer(l));
                sharedPref.setTimesUp(formatTimer(l).toString());
            }

            @Override
            public void onFinish() {

                txtTime.setText("00:00:00");
            }

            public String formatTimer(long millis) {
                output = "";
                seconds = millis / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;

                seconds = seconds % 60;
                minutes = minutes % 60;
                hours = hours % 60;

                String secondsD = String.valueOf(seconds);
                String minutesD = String.valueOf(minutes);
                String hoursD = String.valueOf(hours);

                if (seconds < 10)
                    secondsD = "0" + seconds;
                if (minutes < 10)
                    minutesD = "0" + minutes;

                if (hours < 10)
                    hoursD = "0" + hours;

                output = hoursD + " : " + minutesD + " : " + secondsD;

                return output;
            }
        };
    }

    private List<Question> selectQuestions() {
        questionList = new ArrayList<>();
        Cursor cursor = datasource.queryQuestions(mCategoryId + (1));
//        Log.i("GetCount", cursor.getCount() + "");
        sharedPref.setTotalQuestion(cursor.getCount());
        int maxQuestions = cursor.getCount();
        cursor.moveToFirst();
        int totalQuestions = cursor.getCount();
        if (maxQuestions == 0 || maxQuestions > totalQuestions) {
            maxQuestions = totalQuestions;
        }


        // each question with equal probability, and surprisingly,
        // the cursor never runs off the end.
        int t = 0; // record index
        int m = 0; // records selected so far
        Random r = new Random();
        mQuestions = new Question[maxQuestions];
        for (; m < maxQuestions; t++, cursor.moveToNext()) {
            if (r.nextFloat() * (totalQuestions - t) < maxQuestions - m) {
                mQuestions[m++] = new Question(cursor);
            }
        }

        // Give the questions a shuffle
        for (int i = 0; i < maxQuestions; i++) {
            int j = r.nextInt(maxQuestions);
            Question tmp = mQuestions[i];
            mQuestions[i] = mQuestions[j];
            mQuestions[j] = tmp;
//            Log.i("Questions",mQuestions[j].getQuestion());
//            Log.i("Questions",""+mQuestions[j].getChoices());
//            Log.i("Questions",""+mQuestions[j].getAnswer());
//            Log.i("Questions","================================================");

        }
        questionList = Arrays.asList(mQuestions);

        return questionList;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_activity_exam, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
//            case R.id.action_settings:
//                Toast.makeText(getApplicationContext(), "Settings button clicked", Toast.LENGTH_SHORT).show();
//                break;

        }

        return super.onOptionsItemSelected(item);
    }

    public List<Fragment> getAllFragment() {
        List<Fragment> fList = new ArrayList<Fragment>();
        PlaceholderFragment mf = new PlaceholderFragment();
        try {
            for (int i = 0; i < selectQuestions().size(); i++) {
                fList.add(mf.newInstance(i + (1), selectQuestions().get(i)));
//                            Log.i("TEST", selectQuestions().get(i).getQuestion());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fList;
    }


    public interface OnFragmentInteractionListener {
        public void onFragmentCreated(boolean isDisabled);
    }
    @Override
    public void onFragmentCreated(int number, boolean isCorrect, boolean isClear, boolean isAutoNext, boolean stopTimer) {
        if (stopTimer) {
            countDownTimer.cancel();
        }


        if (isClear) {
            intCorrect = 0;
            intWrong = 0;
        } else {
            if (isCorrect) {
                txtCorrect.setText(String.valueOf(intCorrect += (number)));
                canAutoNext(isAutoNext);
            } else {
                canAutoNext(isAutoNext);
                txtWrong.setText(String.valueOf(intWrong += (number)));

//                Log.i("txtWrong", txtWrong.getText().toString() + intWrong);
            }
        }
    }

    private void canAutoNext(boolean isAutoNext) {
        if (isAutoNext) {
            /*mViewPager.setCurrentItem(currentPage + (1), true);*/
            examTimeStatement();

        }
    }

    @Override
    public void onBackPressed() {
        exitApp();
//        super.onBackPressed();
    }

    private void exitApp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Exam.this);
        alertDialogBuilder.setTitle("Close Exam");
        alertDialogBuilder
                .setMessage("Do you really want to close exam ?")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        intWrong = 0;
                        intCorrect = 0;
//                        checkTimer();
                        countDownTimer.cancel();
                        Score.setAnswered(0);
                        Score.setCorrect(0);
                        Score.setWrong(0);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

}
