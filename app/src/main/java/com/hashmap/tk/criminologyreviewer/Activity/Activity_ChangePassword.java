package com.hashmap.tk.criminologyreviewer.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Crypto.AES_RIJNDAEL;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class Activity_ChangePassword extends AppCompatActivity {
    Button btnChangePassword;
    EditText edtOldPassword, edtPassword, edtCPassword;
    AES_RIJNDAEL crpyt;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private Dialog progressDialog;
    private String cpassword;
    private String password;
    private String username;
    private String oldpassword;
    private SharedPref sharedPref;
    private AES_RIJNDAEL crypt;
    private ParseUser parseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Password");
        cd = new ConnectionDetector(Activity_ChangePassword.this);
        crypt = new AES_RIJNDAEL();

        edtOldPassword = (EditText) findViewById(R.id.edtOldPassword);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtCPassword = (EditText) findViewById(R.id.edtCPassword);
        btnChangePassword = (Button) findViewById(R.id.btnChangePassword);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePassword();
            }
        });
        sharedPref = new SharedPref(Activity_ChangePassword.this);
    }

    public void ChangePassword() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            // Reset errors.
            edtOldPassword.setError(null);
            edtPassword.setError(null);
            edtCPassword.setError(null);

            // Store values at the time of the login attempt.
            oldpassword = edtOldPassword.getText().toString();
            password = edtPassword.getText().toString();
            cpassword = edtCPassword.getText().toString();
//            idnumber = edtIDNumber.getText().toString();

            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(oldpassword)) {
                edtOldPassword.setError(getString(R.string.error_field_required));
                focusView = edtOldPassword;
                cancel = true;
            } else {
                if (oldpassword.equals(sharedPref.getKeyPassword())) {
                    // Check for a valid password, if the user entered one.
                    if (TextUtils.isEmpty(password)) {
                        edtPassword.setError(getString(R.string.error_invalid_password));
                        focusView = edtPassword;
                        cancel = true;
                    } else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                        edtPassword.setError(getString(R.string.error_invalid_password));
                        focusView = edtPassword;
                        cancel = true;
                    }
                    // Check for a valid password, if the user entered one.
                    if (TextUtils.isEmpty(password)) {
                        edtCPassword.setError(getString(R.string.error_invalid_password));
                        focusView = edtCPassword;
                        cancel = true;
                    } else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                        edtCPassword.setError(getString(R.string.error_invalid_password));
                        focusView = edtCPassword;
                        cancel = true;
                    } else if (!cpassword.equals(password)) {
                        edtCPassword.setError(getString(R.string.error_password_not_match));
                        focusView = edtCPassword;
                        cancel = true;
                    }
                } else {
                    edtOldPassword.setError("Old Password not match!");
                    focusView = edtOldPassword;
                    cancel = true;
                }

            }


            if (cancel) {
                focusView.requestFocus();
            } else {
                progressDialog = ProgressDialog.show(
                        Activity_ChangePassword.this, "Change Password", "Changing password...", true);
                ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
                parseQuery.getInBackground(sharedPref.getKeyObjectid(), new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            try {
                                object.put("CryptPass", crypt.encrypt(password, "$up3ru$3r"));
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            object.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        progressDialog.dismiss();
                                        sharedPref = new SharedPref(Activity_ChangePassword.this);
                                        sharedPref.setKeyPassword(password);
                                        Toast.makeText(Activity_ChangePassword.this, "Password successfully changed!", Toast.LENGTH_LONG).show();
                                        onBackPressed();
                                    } else {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {

                        }
                    }
                });
//                parseUser = ParseUser.getCurrentUser();
//                if (parseUser.getUsername() != null) {
//                    username = parseUser.getUsername();
//                    parseUser.setPassword(password);
//                    try {
//                        parseUser.put("CryptPass", crypt.encrypt(password, "$up3ru$3r"));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    parseUser.saveInBackground(new SaveCallback() {
//                        @Override
//                        public void done(ParseException e) {
//                            if (e == null) {
//                                parseUser.logInInBackground(username, password, new LogInCallback() {
//                                    @Override
//                                    public void done(ParseUser user, ParseException e) {
//                                        if (e == null) {
//                                            Log.i("Password", String.valueOf(parseUser.getObjectId()));
//                                            progressDialog.dismiss();
//                                            sharedPref = new SharedPref(Activity_ChangePassword.this);
//                                            sharedPref.setKeyPassword(password);
//                                            Toast.makeText(Activity_ChangePassword.this, "Password successfully changed!", Toast.LENGTH_LONG).show();
//                                            onBackPressed();
//                                        } else {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                });
//
//                            } else {
//                                progressDialog.dismiss();
//                                showAlert("Failed",
//                                        e.getMessage().toString());
//                            }
//                        }
//                    });
//                } else {
//                    ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
//                    parseQuery.getInBackground(sharedPref.getKeyObjectid(), new GetCallback<ParseObject>() {
//                        @Override
//                        public void done(ParseObject object, ParseException e) {
//                            if (e == null) {
//                                try {
//                                    object.put("CryptPass", crypt.encrypt(password, "$up3ru$3r"));
//                                } catch (Exception e1) {
//                                    e1.printStackTrace();
//                                }
//                                object.saveInBackground(new SaveCallback() {
//                                    @Override
//                                    public void done(ParseException e) {
//                                        if (e == null) {
//                                            Log.i("Password", String.valueOf(parseUser.getObjectId()));
//                                            progressDialog.dismiss();
//                                            sharedPref = new SharedPref(Activity_ChangePassword.this);
//                                            sharedPref.setKeyPassword(password);
//                                            Toast.makeText(Activity_ChangePassword.this, "Password successfully changed!", Toast.LENGTH_LONG).show();
//                                            onBackPressed();
//                                        } else {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                });
//                            } else {
//
//                            }
//                        }
//                    });
//                }
            }

        } else {
            showAlert("No Internet Connection", "You don't have internet connection.");
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    public void showAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(Activity_ChangePassword.this).create();
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // Setting alert dialog icon
        alertDialog.setIcon(R.mipmap.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
