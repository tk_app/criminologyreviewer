package com.hashmap.tk.criminologyreviewer.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Database.Datasource;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.Model.Question;
import com.hashmap.tk.criminologyreviewer.Model.Result;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class Activity_ScoreBoard extends AppCompatActivity implements OnClickListener {
    Intent intent;
    TextView txtTotalQuestion,
            txtTimeLimit,
            txtTotalAnswered,
            txtTotalCorrectAnswer,
            txtTotalWrongAnswer,
            txtTimesUp,
            txtTotalItem,
            txtScore;
    Button btnSaveResult,
            btnRetake,
            btnReview;
    Datasource datasource;
    private Double answeredPercent;
    private Double wrongPercent;
    private Double correctPercent;
    private String timeLimit;
    private ParseObject ob;
    private ParseUser currentUser;
    private SharedPref sharedPref;
    private int total;
    private int answered;
    private int correct;
    private int wrong;
    private LinearLayout linearItems, linearHoriz;
    private List<Result> question;
    private TextView lblQ, lblA, lblYA, txtQ, txtA, txtYA;
    private boolean isSaved = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        sharedPref = new SharedPref(Activity_ScoreBoard.this);
        txtTotalQuestion = (TextView) findViewById(R.id.txtTotalQuestion);
        txtTimeLimit = (TextView) findViewById(R.id.txtTimeLimit);
        txtTotalAnswered = (TextView) findViewById(R.id.txtTotalAnswered);
        txtTotalCorrectAnswer = (TextView) findViewById(R.id.txtTotalCorrectAnswer);
        txtTotalWrongAnswer = (TextView) findViewById(R.id.txtTotalWrongAnswer);
        txtTimesUp = (TextView) findViewById(R.id.txtTimesUp);
        btnSaveResult = (Button) findViewById(R.id.btnSave);
        btnRetake = (Button) findViewById(R.id.btnRetake);
        btnReview = (Button) findViewById(R.id.btnReview);
        linearItems = (LinearLayout) findViewById(R.id.linearItems);

        txtTotalItem = (TextView) findViewById(R.id.txtTotalItem);
        txtScore = (TextView) findViewById(R.id.txtScore);

        datasource = new Datasource(Activity_ScoreBoard.this);
        datasource.open();
        question = new ArrayList<>();
        question = datasource.getResult();

        btnSaveResult.setOnClickListener(this);
        btnRetake.setOnClickListener(this);
        btnReview.setOnClickListener(this);

        intent = getIntent();
        total = intent.getIntExtra("totalquestion", 0);
        answered = intent.getIntExtra("totalanswered", 0);
        correct = intent.getIntExtra("correct", 0);
        wrong = intent.getIntExtra("wrong", 0);
        timeLimit = intent.getStringExtra("timelimit");
        answeredPercent = (double) ((100 * answered) / total);
        correctPercent = (double) ((100 * correct) / total);
        wrongPercent = (double) ((100 * wrong) / total);
//        System.out.println(answeredPercent);
//        System.out.println(correctPercent);
//        System.out.println(wrongPercent);

        if (Integer.parseInt(timeLimit) / 1000 / 60 == 3600000) {
            timeLimit = String.valueOf(Integer.parseInt(timeLimit) / 1000 / 60);
            txtTimeLimit.setText(timeLimit + " hr");
            txtTimesUp.setText(Question.getTime());

        } else if (Integer.parseInt(timeLimit) == 0) {
            txtTimeLimit.setText("00:00:00");
            txtTimesUp.setText("Not Set");
        } else {
            timeLimit = String.valueOf(Integer.parseInt(timeLimit) / 1000 / 60);
            txtTimeLimit.setText(timeLimit + " mins");
            txtTimesUp.setText(Question.getTime());
        }
        txtTotalQuestion.setText(String.valueOf(total));
//        txtTimeLimit.setText(timeLimit);
        txtTotalAnswered.setText(answered + " (" + answeredPercent + " %)");
        txtTotalCorrectAnswer.setText(correct + " (" + correctPercent + " %)");
        txtTotalWrongAnswer.setText(wrong + " (" + wrongPercent + " %)");
        txtTimesUp.setText(sharedPref.getTimeTimesUp());

        txtTotalItem.setText(String.valueOf(total) + "/" + String.valueOf(total));
        txtScore.setText(correct + " (" + correctPercent + " %)");
        loadResult();
    }


    public void loadResult() {
        for (int i = 0; i < question.size(); i++) {
//            linearItems = new LinearLayout(this);
            lblQ = new TextView(this);
            lblA = new TextView(this);
            lblYA = new TextView(this);
            TextView div = new TextView(this);
            lblQ.setText("Question : \n" + question.get(i).getQuestion().toString());
            lblYA.setText("Your Answer : " + question.get(i).getYourAnswerWord().toString());
            lblA.setText("Correct Answer : " + question.get(i).getAnswerWord().toString());
            div.setText("------------------------------------");
//            System.out.println("Question : " + question.get(i).getQuestion().toString());
//            System.out.println("Your Answer : " + question.get(i).getYourAnswerWord().toString());
//            System.out.println("Correct Answer : "+question.get(i).getAnswerWord().toString());
//            System.out.println("------------------------------------");
            linearItems.addView(lblQ);
            linearItems.addView(lblYA);
            linearItems.addView(lblA);
            linearItems.addView(div);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                final ProgressDialog progressDialog = new ProgressDialog(Activity_ScoreBoard.this);
                progressDialog.setTitle("Save Score");
                progressDialog.setMessage("Saving score...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();
                currentUser = ParseUser.getCurrentUser();
                ParseObject myComment = new ParseObject("ScoreBoard");
                myComment.put("category_id", String.valueOf(Category.getCategoryId() + 1));
                myComment.put("owner", sharedPref.getKeyObjectid());
                myComment.put("time", timeLimit);
                myComment.put("timelaps", sharedPref.getTimeTimesUp());
                myComment.put("correct", String.valueOf(correct));
                myComment.put("wrong", String.valueOf(wrong));
                myComment.put("highscore", "" + correctPercent);
                myComment.saveInBackground(new SaveCallback() {

                    @Override
                    public void done(ParseException pe) {
                        progressDialog.dismiss();
                        if (pe == null) {
                            Activity_Main.fragment = null;
                            datasource.deleteResult();
                            finish();
                            Toast.makeText(getApplicationContext(), "Score successfully saved.", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "SAVE :" + pe.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
//                ParseQuery<ParseObject> query = ParseQuery.getQuery("ScoreBoard");
//                query.whereEqualTo("owner", ParseObject.createWithoutData("_User", currentUser.getObjectId()));
//                query.whereEqualTo("category_id", String.valueOf(Category.getCategoryId() + 1));
//                query.findInBackground(new FindCallback<ParseObject>() {
//                    @Override
//                    public void done(List<ParseObject> objects, ParseException e) {
//                        if (e == null) {
//                            String aob = null;
//                            try {
//                                aob = objects.get(0).getObjectId();
//                            } catch (Exception e1) {
//                                e1.printStackTrace();
//                            }
//                            if (aob != null) {
//                                ParseQuery<ParseObject> query3 = ParseQuery.getQuery("ScoreBoard");
//                                query3.getInBackground(aob, new GetCallback<ParseObject>() {
//                                    public void done(ParseObject score, ParseException e) {
//                                        if (e == null) {
//                                            score.put("highscore", "" + correctPercent);
//                                            score.put("time", timeLimit);
//                                            score.put("timelaps", sharedPref.getTimeTimesUp());
//                                            score.put("correct", String.valueOf(correct));
//                                            score.put("wrong", String.valueOf(wrong));
//                                            score.saveInBackground(new SaveCallback() {
//                                                @Override
//                                                public void done(ParseException e) {
//                                                    progressDialog.dismiss();
//                                                    if (e == null) {
//                                                        Activity_Main.fragment = null;
//                                                        datasource.deleteResult();
//                                                        finish();
//                                                        Toast.makeText(getApplicationContext(), "Score successfully updated.", Toast.LENGTH_LONG).show();
//                                                    } else {
//                                                        Toast.makeText(getApplicationContext(), "UPDATE :" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
//                                                    }
//                                                }
//                                            });
//                                        }
//                                    }
//                                });
//                            } else {
//                                ParseObject myComment = new ParseObject("ScoreBoard");
//                                myComment.put("category_id", String.valueOf(Category.getCategoryId() + 1));
//                                myComment.put("owner", ParseObject.createWithoutData("_User", currentUser.getObjectId()));
//                                myComment.put("time", timeLimit);
//                                myComment.put("timelaps", sharedPref.getTimeTimesUp());
//                                myComment.put("correct", String.valueOf(correct));
//                                myComment.put("wrong", String.valueOf(wrong));
//                                myComment.put("highscore", "" + correctPercent);
//                                myComment.saveInBackground(new SaveCallback() {
//
//                                    @Override
//                                    public void done(ParseException pe) {
//                                        progressDialog.dismiss();
//                                        if (pe == null) {
//                                            Activity_Main.fragment = null;
//                                            datasource.deleteResult();
//                                            finish();
//                                            Toast.makeText(getApplicationContext(), "Score successfully saved.", Toast.LENGTH_LONG).show();
//                                        } else {
//                                            Toast.makeText(getApplicationContext(), "SAVE :" + pe.getMessage().toString(), Toast.LENGTH_LONG).show();
//                                        }
//                                    }
//                                });
//                            }
//                        } else {
//                            e.printStackTrace();
//                        }
//                    }
//                });

//                    List<ParseObject> ob = query.find();

                break;
            case R.id.btnRetake:
                finish();
                break;
            case R.id.btnReview:
                closeResult(R.id.btnReview);
//                datasource.deleteResult();
//                finish();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                closeResult(android.R.id.home);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeResult(0);
//        if (isSaved) {
//            datasource.deleteResult();
//            super.onBackPressed();
//        } else {
//            closeResult();
//        }
//        datasource.deleteResult();
//        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        datasource.deleteResult();
        super.onDestroy();
    }

    private void closeResult(final int v) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_ScoreBoard.this);
        alertDialogBuilder.setTitle("Result not saved!");
        alertDialogBuilder
                .setMessage("Do you really want to close without saving the result ?")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (v == R.id.btnReview) {
                            Activity_Main.page = 2;
                            finish();
//                            onBackPressed();
                        } else {
                            finish();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
}
