package com.hashmap.tk.criminologyreviewer.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    List<Fragment> fragmentList;

    public SectionsPagerAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return this.fragmentList.get(position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return this.fragmentList.size();
    }


}
