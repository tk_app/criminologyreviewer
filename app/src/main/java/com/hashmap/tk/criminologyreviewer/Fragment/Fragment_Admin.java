package com.hashmap.tk.criminologyreviewer.Fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Activity.Activity_User;
import com.hashmap.tk.criminologyreviewer.Adapter.Adapter_Fragment_AdminPanel;
import com.hashmap.tk.criminologyreviewer.Crypto.AES_RIJNDAEL;
import com.hashmap.tk.criminologyreviewer.Model.Student;
import com.hashmap.tk.criminologyreviewer.Model.User;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Admin extends Fragment implements View.OnClickListener {

    public HashMap<String, ParseObject> map = new HashMap<>();
    public static TextView txtEdit, txtDelete;
    public static CheckBox chkAll;
    public static List<ParseObject> parseUserList;
    public static List<ParseObject> tempparseUserList = new ArrayList<>();
    SharedPref sharedPref;
    private ConnectionDetector cd;
    private ProgressDialog mProgressDialog;
    private ArrayList<Student> resultList;
    private Adapter_Fragment_AdminPanel adapterFragmentAdmin;
    private SwingRightInAnimationAdapter mAnimAdapter;
    private ListView lvListOfUser;
    private Button btnAddNewUser;
    private AES_RIJNDAEL crypt;
    private String password;
    private int i;
    private ParseUser parseUser;
    private String strPassword;
    private int x;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin, container, false);
        map = new HashMap<>();
        cd = new ConnectionDetector(getActivity());
        lvListOfUser = (ListView) view.findViewById(R.id.listUsers);
        btnAddNewUser = (Button) view.findViewById(R.id.btnAddNewUser);
        chkAll = (CheckBox) view.findViewById(R.id.chkAll);
        txtEdit = (TextView) view.findViewById(R.id.txtEdit);
        txtDelete = (TextView) view.findViewById(R.id.txtDelete);
        txtEdit.setOnClickListener(this);
        txtDelete.setOnClickListener(this);
        chkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    tempparseUserList.clear();
                }
            }
        });
        chkAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    adapterFragmentAdmin.checkAll(true);
                } else {
                    adapterFragmentAdmin.checkAll(false);
                }
            }
        });
        lvListOfUser.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {


                return false;
            }
        });
//        if (!User.isNewAdded()) {
//            new ParseDataTask().execute();
//        } else {
//            resultList = User.getStudentList();
//            adapterFragmentAdmin = new Adapter_Fragment_AdminPanel(getActivity(), resultList);
//            mAnimAdapter = new SwingRightInAnimationAdapter(adapterFragmentAdmin);
//            mAnimAdapter.setAbsListView(lvListOfUser);
//            lvListOfUser.setAdapter(mAnimAdapter);
//        }
        btnAddNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Activity_User.class);
                intent.putExtra("tag", "new");
                getActivity().startActivity(intent);
            }
        });

        crypt = new AES_RIJNDAEL();
        sharedPref = new SharedPref(getActivity());
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEdit:
                if (tempparseUserList != null && txtEdit.getText().equals("Edit")) {
                    adapterFragmentAdmin.EditUSer();
                }
                break;
            case R.id.txtDelete:
                if (map != null || map.size() != 0) {
                    for (int i = 0; i < getObjects().size(); i++) {
                        Log.i("User to delete - ", String.valueOf(getObjects().get(i)));
                        System.out.println("--------------------------");
                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setTitle("Delete");
                    alertDialogBuilder
                            .setMessage("Do you really want to delete user/s?")
                            .setIcon(R.mipmap.ic_launcher)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    new DeleteData().execute();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else {
                    Toast.makeText(getActivity(), "Please select user to delete.", Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        chkAll.setChecked(false);
        txtDelete.setEnabled(false);
        txtEdit.setEnabled(false);
        new ParseDataTask().execute();
//        if (User.isNewAdded()) {
//            new ParseDataTask().execute();
//        } else {
//            resultList = User.getStudentList();
//            adapterFragmentAdmin = new Adapter_Fragment_AdminPanel(getActivity(), resultList);
//            mAnimAdapter = new SwingRightInAnimationAdapter(adapterFragmentAdmin);
//            mAnimAdapter.setAbsListView(lvListOfUser);
//            lvListOfUser.setAdapter(mAnimAdapter);
//        }
    }


    public List<ParseObject> getObjects() {
        for (Map.Entry<String, ParseObject> entry : map.entrySet()) {
             tempparseUserList.add(entry.getValue());
//        for (Iterator<Map.Entry<String, ParseObject>> it = map.entrySet().iterator(); it.hasNext(); ) {
//            Map.Entry<String, ParseObject> entry = it.next();
//            value.add(entry.getValue());
        }

        return tempparseUserList;
    }

    public void addObject(String id, ParseObject value) {
        Log.i(id, String.valueOf(value));
        map.put(id, value);
    }

    public void deleteObject(String id) {
        Log.i(id, String.valueOf(id));

        map.remove(id);
    }

    public void showAlertDialog(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if (!(getActivity().isFinishing())) {
            alertDialog.show();
        }

    }

    private class ParseDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("Retrieving all users...");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
            resultList = new ArrayList<Student>();


            try {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Users");
                query.orderByDescending("createdAt");
                query.whereEqualTo("AccessType", "user");
                query.whereEqualTo("Active", true);
                parseUserList = query.find();
                for (ParseObject user : parseUserList) {
                    Student student = new Student();

//                    System.out.println("Username - " + user.getUsername());
//                    System.out.println("Firstname - " + String.valueOf(user.get("Firstname")));
//                    System.out.println("Lastname - " + String.valueOf(user.get("Lastname")));
//                    System.out.println("Middle - " + String.valueOf(user.get("Middle")));
//                    System.out.println("Birthdate - " + (Date) user.get("Birthdate"));
//                    System.out.println("Email - " + user.getEmail());
//                    System.out.println("Section - " + String.valueOf(user.get("Section")));
//                    System.out.println("SY1 - " + String.valueOf(user.get("SY1")));
//                    System.out.println("SY2 - " + String.valueOf(user.get("SY2")));
//                    System.out.println("-----------------------------------------");

                    student.setStudObjectID(user.getObjectId());
                    try {
                        student.setStudStudPassword(crypt.decrypt(String.valueOf(user.get("CryptPass")), "$up3ru$3r"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    student.setStudUsername(String.valueOf(user.get("username")));
                    student.setStudFirstname(String.valueOf(user.get("Firstname")));
                    student.setStudLastname(String.valueOf(user.get("Lastname")));
                    student.setStudMiddle(String.valueOf(user.get("Middle")));
//                    SimpleDateFormat format = new SimpleDateFormat("MMM dd,yyyy");
//                    Date d = format.parse(((Date) user.get("Birthdate")).getDate());
                    Date d = new Date();
                    d = ((Date) user.get("Birthdate"));
                    student.setStudBirthdate(d);
                    student.setStudEmail(String.valueOf(user.get("email")));
//                    student.setStudSection(String.valueOf(user.get("Section")));
//                    student.setStudSY1(String.valueOf(user.get("SY1")));
//                    student.setStudSY2(String.valueOf(user.get("SY2")));
                    resultList.add(student);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            User.setStudentList(resultList);
            User.setIsNewAdded(false);
            adapterFragmentAdmin = new Adapter_Fragment_AdminPanel(getActivity(), resultList);
            mAnimAdapter = new SwingRightInAnimationAdapter(adapterFragmentAdmin);
            mAnimAdapter.setAbsListView(lvListOfUser);
            lvListOfUser.setAdapter(mAnimAdapter);
            chkAll.setChecked(false);
            mProgressDialog.dismiss();


        }
    }

    private class DeleteData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("Delete Result");
            mProgressDialog.setMessage("Deleting...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            SimpleDateFormat formatter = new SimpleDateFormat("MMM.dd.yy");
//            Toast.makeText(getActivity(), "User/s successfully deleted.", Toast.LENGTH_LONG).show();


            ParseObject.deleteAllInBackground(getObjects(), new DeleteCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Toast.makeText(getActivity(), "User/s successfully deleted.", Toast.LENGTH_LONG).show();
                        new ParseDataTask().execute();
                    } else {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgressDialog.dismiss();
            tempparseUserList.clear();
            adapterFragmentAdmin.checkAll(false);
        }
    }

}
