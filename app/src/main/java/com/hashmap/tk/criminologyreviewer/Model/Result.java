package com.hashmap.tk.criminologyreviewer.Model;

public class Result {
    public long id;
    public int category_id;
    public String question;
    public String[] choices;
    public int answer;
    public int yourAnswer;
    public String time;
    public String answerWord;
    public String yourAnswerWord;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String[] getChoices() {
        return choices;
    }

    public void setChoices(String[] choices) {
        this.choices = choices;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int getYourAnswer() {
        return yourAnswer;
    }

    public void setYourAnswer(int yourAnswer) {
        this.yourAnswer = yourAnswer;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAnswerWord() {
        return answerWord;
    }

    public void setAnswerWord(String answerWord) {
        this.answerWord = answerWord;
    }

    public String getYourAnswerWord() {
        return yourAnswerWord;
    }

    public void setYourAnswerWord(String yourAnswerWord) {
        this.yourAnswerWord = yourAnswerWord;
    }
}
