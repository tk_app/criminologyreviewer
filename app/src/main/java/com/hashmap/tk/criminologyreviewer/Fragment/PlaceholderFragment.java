package com.hashmap.tk.criminologyreviewer.Fragment;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hashmap.tk.criminologyreviewer.Activity.Activity_ScoreBoard;
import com.hashmap.tk.criminologyreviewer.Database.Datasource;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.Model.Question;
import com.hashmap.tk.criminologyreviewer.Model.Score;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;

import java.util.List;

public class PlaceholderFragment extends Fragment implements View.OnClickListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    public Question mqQuestions;
    public int selectedAns;
    public int correctAns;
    public int totalQuestion;
    public int currentQuestion;
    public int sectionNumber;
    public int intAnsweredQuestion;
    public int intCorrect;
    public int intWrong;
    public String strTimesUp;
    TextView txtQuestion;
    Button btnAns1, btnAns2, btnAns3, btnAns4;
    SharedPref sharedPref;
    MediaPlayer audioCorrectAns, audioWrongAns;
    Handler handler;
    private OnFragmentInteractionListener listener;
    private boolean isSoundOn;
    private List<String> listQA;
    Datasource datasource;
    String youranswer;
    String answer;
    private boolean isAnswered = false;

    public PlaceholderFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Change Activity State when fragment change views
     */

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            listener.onFragmentCreated(intCorrect+=(1));
//            Log.i("Fragment", "call activity: " + intCorrect);
//        }
//    }
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public PlaceholderFragment newInstance(int sectionNumber, Question questions) {
        mqQuestions = questions;
//        Log.i("TEST", questions.getQuestion());
//        Log.i("TEST", mqQuestions.getQuestion());
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setMqQuestions(questions);
        fragment.setSectionNumber(sectionNumber);

        fragment.setArguments(args);
        return fragment;
    }

    public void setSectionNumber(int sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public void setCurrentQuestion(int currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public void setMqQuestions(Question mqQuestions) {
        this.mqQuestions = mqQuestions;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activity_exam, container, false);
        sharedPref = new SharedPref(getActivity());
//        sharedPref.setTotalQuestion(10);
        datasource = new Datasource(getActivity());
        datasource.open();
        isSoundOn = sharedPref.getSound();
        totalQuestion = sharedPref.getTotalQuestion();
        audioCorrectAns = MediaPlayer.create(getActivity(), R.raw.correct);
        audioWrongAns = MediaPlayer.create(getActivity(), R.raw.incorrect);
        txtQuestion = (TextView) rootView.findViewById(R.id.txtQuestion);
        btnAns1 = (Button) rootView.findViewById(R.id.btnAns1);
        btnAns2 = (Button) rootView.findViewById(R.id.btnAns2);
        btnAns3 = (Button) rootView.findViewById(R.id.btnAns3);
        btnAns4 = (Button) rootView.findViewById(R.id.btnAns4);
//            txtQuestion.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        correctAns = this.mqQuestions.getAnswer();
        txtQuestion.setText(this.mqQuestions.getQuestion());
        btnAns1.setText(this.mqQuestions.getChoices()[0]);
        btnAns2.setText(this.mqQuestions.getChoices()[1]);
        btnAns3.setText(this.mqQuestions.getChoices()[2]);
        btnAns4.setText(this.mqQuestions.getChoices()[3]);
        btnAns1.setOnClickListener(this);
        btnAns2.setOnClickListener(this);
        btnAns3.setOnClickListener(this);
        btnAns4.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (!isAnswered) {
            switch (v.getId()) {
                case R.id.btnAns1:
                    selectedAns = 0;
                    btnAns1.setBackgroundResource(R.drawable.rounded_button_pressed_ans);
                    break;
                case R.id.btnAns2:
                    selectedAns = 1;
                    btnAns2.setBackgroundResource(R.drawable.rounded_button_pressed_ans);
                    break;
                case R.id.btnAns3:
                    selectedAns = 2;
                    btnAns3.setBackgroundResource(R.drawable.rounded_button_pressed_ans);
                    break;
                case R.id.btnAns4:
                    selectedAns = 3;
                    btnAns4.setBackgroundResource(R.drawable.rounded_button_pressed_ans);
                    break;
            }

            boolean isAutoNext = sharedPref.getAutoNext();
            handler = new Handler();

            if (selectedAns == correctAns) {
                Score.setCorrect(Score.getCorrect() + 1);
                Log.i("Correct", "" + Score.getCorrect());
                listener.onFragmentCreated(1, true, false, isAutoNext, false);
                intCorrect = Score.getCorrect();

                if (isSoundOn) {
                    MediaPlayer mp;
                    mp = MediaPlayer.create(getActivity(), R.raw.correct);
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            mp.reset();
                            mp.release();
                            mp = null;
                        }

                    });
                    mp.start();
                }
            } else {
                Score.setWrong(Score.getWrong() + 1);
                Log.i("Wrong", "" + Score.getWrong());
                listener.onFragmentCreated(1, false, false, isAutoNext, false);
                intWrong = Score.getWrong();

                if (isSoundOn) {
                    MediaPlayer mp;
                    mp = MediaPlayer.create(getActivity(), R.raw.incorrect);
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            mp.reset();
                            mp.release();
                            mp = null;
                        }

                    });
                    mp.start();
                }
            }
//        intAnsweredQuestion+=1;
            Score.setAnswered(Score.getAnswered() + 1);
//        Log.i("Answered", "" + Score.getAnswered());
            intAnsweredQuestion = Score.getAnswered();
//        disableButton(false);
//        Log.i("QQQQQQQQQQQQQQ", "" + intAnsweredQuestion + "/" + totalQuestion);
            if (totalQuestion == intAnsweredQuestion) {
                String timesUp = null, timeLimit = null;
                if (sharedPref.getTimeLimit().equals("0")) {
                    timesUp = "Not Set";
                    timeLimit = sharedPref.getTimeLimit();
                } else {
                    timeLimit = sharedPref.getTimeLimit();
                }

                Intent intent = new Intent(getActivity(), Activity_ScoreBoard.class);
                intent.putExtra("totalquestion", totalQuestion);
                intent.putExtra("timelimit", timeLimit);
                intent.putExtra("totalanswered", Score.getAnswered());
                intent.putExtra("correct", Score.getCorrect());
                intent.putExtra("wrong", Score.getWrong());
                intent.putExtra("timesup", Question.getTime());
                intent.putExtra("category_id", String.valueOf(Category.getCategoryId()));
    //            disableButton(false);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
                getActivity().finish();
                Score.setAnswered(0);
                Score.setCorrect(0);
                Score.setWrong(0);
                listener.onFragmentCreated(0, true, true, isAutoNext, true);
                listener.onFragmentCreated(0, false, true, isAutoNext, true);
            }

//        listener.onFragmentCreated(0, false, true, isAutoNext, true);
            answer = this.mqQuestions.getChoices()[correctAns];
            youranswer = this.mqQuestions.getChoices()[selectedAns];

            datasource.addResult(this.mqQuestions.getQuestion(), answer, youranswer);
//        Log.i("Question", this.mqQuestions.getQuestion());
//        Log.i("answer",answer);
//        Log.i("youranswer",youranswer);
            isAnswered = true;
        } else {
//            Toast.makeText(getActivity(),"You can only answer once.",Toast.LENGTH_LONG).show();
        }
    }


    public void disableButton(boolean isCorrect) {
        btnAns1.setEnabled(isCorrect);
        btnAns1.setBackgroundResource(R.drawable.rounded_button_disabled);
        btnAns2.setEnabled(isCorrect);
        btnAns2.setBackgroundResource(R.drawable.rounded_button_disabled);
        btnAns3.setEnabled(isCorrect);
        btnAns3.setBackgroundResource(R.drawable.rounded_button_disabled);
        btnAns4.setEnabled(isCorrect);
        btnAns4.setBackgroundResource(R.drawable.rounded_button_disabled);
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentCreated(int number, boolean isCorrect, boolean isClear, boolean isAutoNext, boolean stopTimer);
    }
}