package com.hashmap.tk.criminologyreviewer.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Crypto.AES_RIJNDAEL;
import com.hashmap.tk.criminologyreviewer.Model.User;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Activity_User extends AppCompatActivity implements OnClickListener, DatePickerDialog.OnDateSetListener {
    Date birthdate;
    TextView txtBirthdateError;
    EditText edtStudNumber, edtPassword,
            edtStudLastname,
            edtStudFirstname,
            edtStudMiddle,
            edtStudSection,
            edtStudEmail,
            edtSY1, edtSY2;
    Button btnAddUser, btnStudBirthdate;
    String TAG = "new";
    AES_RIJNDAEL crypt;
    SharedPref sharedPref;
    private ConnectionDetector cd;
    private boolean isInternetPresent;
    private ProgressDialog pDialog;
    private Intent intent;
    private String strObjectID, strPassword,
            strUsername,
            strLastname,
            strFirstname,
            strMiddle,
            strBirthdate,
            strEmail,
            strSection,
            strSY1,
            strSY2;
    private Calendar calendar;
    private String password, username, lastname, firstname, middle, section, email, sy1, sy2;
    private String pass;
    private String tempObjectID;
    private ParseUser parseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Student Information");
        cd = new ConnectionDetector(this);
        edtStudNumber = (EditText) findViewById(R.id.edtStudNumber);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtStudLastname = (EditText) findViewById(R.id.edtStudLastname);
        edtStudFirstname = (EditText) findViewById(R.id.edtStudFirstname);
        edtStudMiddle = (EditText) findViewById(R.id.edtStudMiddle);
        edtStudSection = (EditText) findViewById(R.id.edtStudSection);
        edtStudEmail = (EditText) findViewById(R.id.edtStudEmail);
        edtSY1 = (EditText) findViewById(R.id.edtSY1);
        edtSY2 = (EditText) findViewById(R.id.edtSY2);
        btnAddUser = (Button) findViewById(R.id.btnAddUser);
        btnStudBirthdate = (Button) findViewById(R.id.btnStudBirthdate);
        txtBirthdateError = (TextView) findViewById(R.id.txtBirthdateError);
        btnAddUser.setOnClickListener(this);
        btnStudBirthdate.setOnClickListener(this);
        intent = getIntent();
        TAG = intent.getStringExtra("tag");
        strObjectID = intent.getStringExtra("objectid");
        strUsername = intent.getStringExtra("username");
        strPassword = intent.getStringExtra("password");
        strLastname = intent.getStringExtra("lastname");
        strFirstname = intent.getStringExtra("firstname");
//        strMiddle = intent.getStringExtra("middle");
//        strBirthdate = intent.getStringExtra("birthdate");
//        strEmail = intent.getStringExtra("email");
//        strSection = intent.getStringExtra("section");
        strSY1 = intent.getStringExtra("sy1");
        strSY2 = intent.getStringExtra("sy2");
        crypt = new AES_RIJNDAEL();
        sharedPref = new SharedPref(Activity_User.this);
        try {
            System.out.println(TAG);
            System.out.println(strUsername);
            System.out.println(strLastname);
            System.out.println(strFirstname);
            System.out.println(strMiddle);
            System.out.println(strBirthdate);
            System.out.println(strEmail);
            System.out.println(strSection);
            System.out.println(strSY1);
            System.out.println(strSY2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TAG.equals("update")) {

                        try {
                            strPassword = crypt.decrypt(strPassword, "$up3ru$3r");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        edtStudNumber.setText(strUsername);
                        edtPassword.setText(strPassword);
                        edtStudLastname.setText(strLastname);
                        edtStudFirstname.setText(strFirstname);
//                        edtStudMiddle.setText(strMiddle);
//                        edtStudSection.setText(strSection);
//                        edtStudEmail.setText(strEmail);
                        edtSY1.setText(strSY1);
                        edtSY2.setText(strSY2);
                        btnStudBirthdate.setText(strBirthdate);
                        btnAddUser.setText("Update");



        } else {
            edtStudLastname.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        edtPassword.setText(edtStudLastname.getText().toString().toLowerCase() + edtStudNumber.getText().toString().substring(edtStudNumber.getText().toString().length() - 2));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            btnAddUser.setText("Add User");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddUser:
                if (TAG.equals("new")) {
                    AddUser();
                } else if (TAG.equals("update")) {
                    UpdateUser();
                }
                break;
            case R.id.btnStudBirthdate:
                calendar = Calendar.getInstance();

                DatePickerDialog dpd = DatePickerDialog.newInstance(Activity_User.this,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));

                dpd.setThemeDark(false);
                dpd.vibrate(false);
                dpd.dismissOnPause(true);
                dpd.setTitle("Select Date");
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        String Date = new SimpleDateFormat("MMM").format(calendar.getTime()) + " ";
                        Date += new SimpleDateFormat("dd").format(calendar.getTime()) + ", ";
                        Date += new SimpleDateFormat("yyyy").format(calendar.getTime());
                        birthdate = new Date();
                        birthdate.setTime(calendar.getTimeInMillis());
                        Log.i("Birthdate", String.valueOf(birthdate));
                        btnStudBirthdate.setText(Date);
                        txtBirthdateError.setVisibility(View.INVISIBLE);

                    }
                });
                dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.cancel();
                    }
                });
                dpd.show(getFragmentManager(), "Timepickerdialog");
                break;

        }
    }

    public void AddUser() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            // Reset errors.
            edtStudNumber.setError(null);
            edtStudLastname.setError(null);
            edtPassword.setError(null);
            edtStudFirstname.setError(null);
//            edtStudMiddle.setError(null);
            edtStudSection.setError(null);
//            edtStudEmail.setError(null);
            edtSY1.setError(null);
            edtSY2.setError(null);

            // Store values at the time of the login attempt.
            password = edtPassword.getText().toString();
            username = edtStudNumber.getText().toString();
            lastname = edtStudLastname.getText().toString();
            firstname = edtStudFirstname.getText().toString();
//            String birthdate = btnStudBirthdate.getText().toString();
//            middle = edtStudMiddle.getText().toString();
            section = edtStudSection.getText().toString();
//            email = edtStudEmail.getText().toString();
//            sy1 = edtSY1.getText().toString();
//            sy2 = edtSY2.getText().toString();

            boolean cancel = false;
            View focusView = null;


            if (TextUtils.isEmpty(username)) {
                edtStudNumber.setError(getString(R.string.error_field_required));
                focusView = edtStudNumber;
                cancel = true;
            }
            if (TextUtils.isEmpty(lastname)) {
                edtStudLastname.setError(getString(R.string.error_field_required));
                focusView = edtStudLastname;
                cancel = true;
            }
            if (TextUtils.isEmpty(firstname)) {
                edtStudFirstname.setError(getString(R.string.error_field_required));
                focusView = edtStudFirstname;
                cancel = true;
            }
//            if (TextUtils.isEmpty(middle)) {
//                edtStudMiddle.setError(getString(R.string.error_field_required));
//                focusView = edtStudMiddle;
//                cancel = true;
//            }
//            if (birthdate == null) {
//                txtBirthdateError.setVisibility(View.VISIBLE);
//                focusView = txtBirthdateError;
//                cancel = true;
//            }
//            if (TextUtils.isEmpty(section)) {
//                edtStudSection.setError(getString(R.string.error_field_required));
//                focusView = edtStudSection;
//                cancel = true;
//            }
//            if (TextUtils.isEmpty(email)) {
//                edtStudEmail.setError(getString(R.string.error_field_required));
//                focusView = edtStudEmail;
//                cancel = true;
//            }
            if (TextUtils.isEmpty(password)) {
                edtPassword.setError(getString(R.string.error_field_required));
                focusView = edtPassword;
                cancel = true;
            }
//            if (TextUtils.isEmpty(sy1)) {
//                edtSY1.setError(getString(R.string.error_field_required));
//                focusView = edtSY1;
//                cancel = true;
//            }
//            if (TextUtils.isEmpty(sy2)) {
//                edtSY2.setError(getString(R.string.error_field_required));
//                focusView = edtSY2;
//                cancel = true;
//            }

            if (cancel) {
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true, "Add User", "Adding user...");

//                pass = lastname.toLowerCase() + username.substring(username.length() - 2);
//                ParseUser parseUser = new ParseUser();
                ParseObject parseObject = new ParseObject("Users");
                parseObject.put("username", username);
//                parseObject.put("password", password);
//                parseObject.put("email", email);
                parseObject.put("Firstname", firstname);
//                parseObject.put("Middle", middle);
                parseObject.put("Lastname", lastname);
//                parseObject.put("Birthdate", birthdate);
//                parseObject.put("Section", section);
//                parseObject.put("SY1", sy1);
//                parseObject.put("SY2", sy2);
                parseObject.put("AccessType", "user");
                parseObject.put("Active", true);
                try {
                    password = crypt.encrypt(password, "$up3ru$3r");
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                try {
                    parseObject.put("CryptPass", password);
                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                btnAddUser.setText("Add User");
                                User.setIsNewAdded(false);
                                User.setStudentList(null);
                                showProgress(false, null, null);
                                finish();
                                Toast.makeText(Activity_User.this, "User successfully added!", Toast.LENGTH_LONG).show();
//                                ParseUser.logInInBackground(sharedPref.getKeyUsername(), sharedPref.getKeyPassword(), new LogInCallback() {
//                                    @Override
//                                    public void done(ParseUser user, ParseException e) {
//                                        if (e == null) {
//                                            tempObjectID = user.getObjectId();
//                                            showProgress(false, null, null);
//                                            Toast.makeText(Activity_User.this, "User successfully added!", Toast.LENGTH_LONG).show();
//                                            finish();
//                                        } else {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                });
                            } else {
                                showProgress(false, null, null);
                                showAlertDialog("Failed", e.getMessage().toString());
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(Activity_User.this).create();
            alertDialog.setTitle("No Internet Connection");
            alertDialog.setMessage("You don't have internet connection.");
            alertDialog.setIcon(R.mipmap.ic_launcher);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }


    }

    public void UpdateUser() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            // Reset errors.
            edtPassword.setError(null);
            edtStudNumber.setError(null);
            edtStudLastname.setError(null);
            edtStudFirstname.setError(null);
//            edtStudMiddle.setError(null);
//            edtStudSection.setError(null);
//            edtStudEmail.setError(null);
            edtSY1.setError(null);
            edtSY2.setError(null);

            // Store values at the time of the login attempt.
            password = edtPassword.getText().toString();
            username = edtStudNumber.getText().toString();
            lastname = edtStudLastname.getText().toString();
            firstname = edtStudFirstname.getText().toString();
//            String birthdate = btnStudBirthdate.getText().toString();
            middle = edtStudMiddle.getText().toString();
            section = edtStudSection.getText().toString();
//            email = edtStudEmail.getText().toString();
            sy1 = edtSY1.getText().toString();
            sy2 = edtSY2.getText().toString();
//            pass = lastname.toLowerCase() + username.substring(username.length() - 2);
            pass = edtPassword.getText().toString();

            boolean cancel = false;
            View focusView = null;


            if (TextUtils.isEmpty(username)) {
                edtStudNumber.setError(getString(R.string.error_field_required));
                focusView = edtStudNumber;
                cancel = true;
            }
            if (TextUtils.isEmpty(lastname)) {
                edtStudLastname.setError(getString(R.string.error_field_required));
                focusView = edtStudLastname;
                cancel = true;
            }
            if (TextUtils.isEmpty(firstname)) {
                edtStudFirstname.setError(getString(R.string.error_field_required));
                focusView = edtStudFirstname;
                cancel = true;
            }
//            if (TextUtils.isEmpty(middle)) {
//                edtStudMiddle.setError(getString(R.string.error_field_required));
//                focusView = edtStudMiddle;
//                cancel = true;
//            }
//            if (birthdate == null) {
//                txtBirthdateError.setVisibility(View.VISIBLE);
//                focusView = txtBirthdateError;
//                cancel = true;
//            }
//            if (TextUtils.isEmpty(section)) {
//                edtStudSection.setError(getString(R.string.error_field_required));
//                focusView = edtStudSection;
//                cancel = true;
//            }
//            if (TextUtils.isEmpty(email)) {
//                edtStudEmail.setError(getString(R.string.error_field_required));
//                focusView = edtStudEmail;
//                cancel = true;
//            }
            if (TextUtils.isEmpty(password)) {
                edtPassword.setError(getString(R.string.error_field_required));
                focusView = edtPassword;
                cancel = true;
            }
//            if (TextUtils.isEmpty(sy1)) {
//                edtSY1.setError(getString(R.string.error_field_required));
//                focusView = edtSY1;
//                cancel = true;
//            }
//            if (TextUtils.isEmpty(sy2)) {
//                edtSY2.setError(getString(R.string.error_field_required));
//                focusView = edtSY2;
//                cancel = true;
//            }

            if (cancel) {
                focusView.requestFocus();
            } else {
                showProgress(true, "Update User", "Updating user information...");
//                parseUser.getCurrentUser().logOut();
                Log.i("Login", strUsername);
                Log.i("Login", strPassword);

                ParseQuery<ParseObject>parseQuery = ParseQuery.getQuery("Users");
                parseQuery.getInBackground(strObjectID, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            object.put("username", username);
//                            object.put("password", password);
//                            object.put("email", email);
                            object.put("Firstname", firstname);
//                            object.put("Middle", middle);
                            object.put("Lastname", lastname);
//                            object.put("Birthdate", birthdate);
//                            object.put("Section", section);
//                            object.put("SY1", sy1);
//                            object.put("SY2", sy2);
                            object.put("AccessType", "user");
                            object.put("Active", true);
                            try {
                                password = crypt.encrypt(password, "$up3ru$3r");
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }

                            object.put("CryptPass", password);
                            object.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    btnAddUser.setText("Add User");
                                    showProgress(false, null, null);
                                    User.setIsNewAdded(true);
                                    User.setStudentList(null);
                                    Log.i("getKeyUsername", sharedPref.getKeyUsername());
                                    Log.i("getKeyPassword", sharedPref.getKeyPassword());
                                    showProgress(false, null, null);
                                    Toast.makeText(Activity_User.this, "User successfully updated!", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            });
                        }
                    }
                });
                /*ParseUser.logInInBackground(strUsername, strPassword, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if (e == null) {
                            Log.i("Login", "Login");

                            user = ParseUser.getCurrentUser();
                            user.setUsername(username);
                            user.setPassword(pass);
                            user.setEmail(email);
                            user.put("Firstname", firstname);
                            user.put("Middle", middle);
                            user.put("Lastname", lastname);
                            user.put("Birthdate", birthdate);
                            user.put("Section", section);
                            user.put("SY1", sy1);
                            user.put("SY2", sy2);
                            user.put("AccessType", "user");
                            try {
                                user.put("CryptPass", crypt.encrypt(pass, "$up3ru$3r"));
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {

                                        parseUser.logInInBackground(sharedPref.getKeyUsername(), sharedPref.getKeyPassword(), new LogInCallback() {
                                            @Override
                                            public void done(ParseUser user, ParseException e) {
                                                if (e == null) {
                                                    Log.i("LoginCallback", "Successfully logged in user.");
                                                    Log.i("Update", "User");
                                                    btnAddUser.setText("Add User");
                                                    showProgress(false, null, null);
                                                    User.setIsNewAdded(true);
                                                    User.setStudentList(null);
                                                    Log.i("getKeyUsername", sharedPref.getKeyUsername());
                                                    Log.i("getKeyPassword", sharedPref.getKeyPassword());
                                                    showProgress(false, null, null);
                                                    Toast.makeText(Activity_User.this, "User successfully updated!", Toast.LENGTH_LONG).show();
                                                    finish();
//                                                    ParseQuery<ParseObject> queryPassword = ParseQuery.getQuery("Password");
//                                                    queryPassword.whereEqualTo("owner", ParseObject.createWithoutData("_User", strObjectID));
//                                                    queryPassword.getFirstInBackground(new GetCallback<ParseObject>() {
//                                                        @Override
//                                                        public void done(ParseObject object, ParseException e) {
//                                                            if (e == null) {
//                                                                Log.i("QueryPasswordCallback", "Successfully get password object.");
//
//                                                                Log.i("Password", String.valueOf(object.getObjectId()));
//                                                                try {
//                                                                    object.put("Password", crypt.encrypt(pass, "$up3ru$3r"));
//                                                                } catch (Exception e1) {
//                                                                    e1.printStackTrace();
//                                                                }
//                                                                object.saveInBackground(new SaveCallback() {
//                                                                    @Override
//                                                                    public void done(ParseException e) {
//                                                                        if (e == null) {
//                                                                            Log.i("SavePasswordCallback", "Successfully saved password.");
//
//                                                                            ParseUser.logInInBackground(sharedPref.getKeyUsername(), sharedPref.getKeyPassword(), new LogInCallback() {
//                                                                                @Override
//                                                                                public void done(ParseUser user, ParseException e) {
//                                                                                    if (e == null) {
//                                                                                        Log.i("LoginCallback", "Successfully logged in admin.");
//                                                                                        showProgress(false, null, null);
//                                                                                        Toast.makeText(Activity_User.this, "User successfully updated!", Toast.LENGTH_LONG).show();
//                                                                                        finish();
//                                                                                    }
//                                                                                }
//                                                                            });
//                                                                        } else {
//                                                                            e.printStackTrace();
//                                                                        }
//                                                                    }
//                                                                });
//                                                            } else {
//                                                                e.printStackTrace();
//                                                            }
//                                                        }
//                                                    });
                                                }
                                            }
                                        });


//
                                    } else {
                                        showProgress(false, null, null);
                                        showAlertDialog("Failed", e.getMessage().toString());
                                    }
                                }
                            });


                        } else {
                            showProgress(false, null, null);
                            e.printStackTrace();
                        }
                    }
                });*/


            }


        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(Activity_User.this).create();
            alertDialog.setTitle("No Internet Connection");
            alertDialog.setMessage("You don't have internet connection.");
            alertDialog.setIcon(R.mipmap.ic_launcher);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }


    }

    private void showProgress(final boolean show, String title, String message) {
        if (show) {
            pDialog = new ProgressDialog(Activity_User.this);
            pDialog.setIndeterminate(true);
            pDialog.setTitle(title);
            pDialog.setMessage(message);
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            pDialog.dismiss();
        }

    }

    public void showAlertDialog(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(Activity_User.this)
                .create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        //    alertDialog.setIcon(R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        if (!(Activity_User.this.isFinishing())) {
            // show dialog
            alertDialog.show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
//            case R.id.action_settings:
//                Toast.makeText(getApplicationContext(), "Settings button clicked", Toast.LENGTH_SHORT).show();
//                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

    }
}
