package com.hashmap.tk.criminologyreviewer.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hashmap.tk.criminologyreviewer.Activity.Activity_Exam;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.R;

import java.util.List;


public class Adapter_Fragment_Exam extends RecyclerView.Adapter<Adapter_Fragment_Exam.ViewHolder> {

    private final List<Category> categoryList;
    Context context;
    public Adapter_Fragment_Exam(Context context,List<Category> items) {
        categoryList = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.txtTitle.setText(categoryList.get(position).getTitle());
        holder.txtSet.setText(categoryList.get(position).getSet());
        holder.txtPercent.setText(String.valueOf(categoryList.get(position).getHighscore()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Recycler", categoryList.get(position).getTitle());
                Intent intent = new Intent(context.getApplicationContext(), Activity_Exam.class);
                intent.putExtra("categoryID",position);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtTitle;
        public final TextView txtSet;
        public final TextView txtPercent;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            txtSet = (TextView) view.findViewById(R.id.txtSet);
            txtPercent = (TextView) view.findViewById(R.id.txtPercent);
        }


    }
}
