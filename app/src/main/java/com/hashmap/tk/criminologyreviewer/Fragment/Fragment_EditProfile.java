package com.hashmap.tk.criminologyreviewer.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Activity.Activity_ChangePassword;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class Fragment_EditProfile extends Fragment {
    EditText edtFirstname,
            edtLastname,
            edtEmail,
            edtUsername;
    Button btnUpdateProfile;
    TextView txtChangePassword;
    private String firstname;
    private String lastname;
    private String email;
    private String username;
    private boolean isInternetPresent;
    private ConnectionDetector cd;
    private UpdateProfileTask mAuthTask;
    private ProgressDialog progressDialog;
    private ParseUser parseUser;
    private SharedPref sharedPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        cd = new ConnectionDetector(getActivity());
        sharedPref = new SharedPref(getActivity());
        edtFirstname = (EditText) view.findViewById(R.id.edtFirstname);
        edtLastname = (EditText) view.findViewById(R.id.edtLastname);
        edtEmail = (EditText) view.findViewById(R.id.edtEmail);
        edtUsername = (EditText) view.findViewById(R.id.edtUsername);
        btnUpdateProfile = (Button) view.findViewById(R.id.btnUpdateProfile);
        txtChangePassword = (TextView) view.findViewById(R.id.txtChangePassword);

//        parseUser = ParseUser.getCurrentUser();
        try {
            progressDialog = ProgressDialog.show(
                    getActivity(), "", "Loading profile...", true);
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
            parseQuery.getInBackground(sharedPref.getKeyObjectid(), new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        edtFirstname.setText(object.get("Firstname").toString());
                        edtLastname.setText(object.get("Lastname").toString());
//                        edtEmail.setText(object.get("email").toString());
                        edtUsername.setText(object.get("username").toString());
                        progressDialog.dismiss();
                    } else {
                        e.printStackTrace();
                    }

                }
            });
//            if (parseUser.getObjectId() != null) {
//                edtFirstname.setText((CharSequence) parseUser.get("Firstname"));
//                edtLastname.setText((CharSequence) parseUser.get("Lastname"));
//                edtEmail.setText(parseUser.getEmail());
//                edtUsername.setText(parseUser.getUsername());
//            } else {
//                progressDialog = ProgressDialog.show(
//                        getActivity(), "", "Loading profile...", true);
//                ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
//                parseQuery.getInBackground(sharedPref.getKeyObjectid(), new GetCallback<ParseObject>() {
//                    @Override
//                    public void done(ParseObject object, ParseException e) {
//                        if (e == null) {
//                            edtFirstname.setText(object.get("Firstname").toString());
//                            edtLastname.setText(object.get("Lastname").toString());
//                            edtEmail.setText(object.get("email").toString());
//                            edtUsername.setText(object.get("username").toString());
//                            progressDialog.dismiss();
//                        } else {
//                            e.printStackTrace();
//                        }
//
//                    }
//                });
//
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        txtChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Activity_ChangePassword.class));
            }
        });
        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(btnUpdateProfile.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);
                UpdateProfile();
            }
        });
        sharedPref = new SharedPref(getActivity());

        return view;
    }

    public void UpdateProfile() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            // Reset errors.
            edtFirstname.setError(null);
            edtLastname.setError(null);
            edtUsername.setError(null);

            // Store values at the time of the login attempt.
            firstname = edtFirstname.getText().toString();
            lastname = edtLastname.getText().toString();
//            email = edtEmail.getText().toString();
            username = edtUsername.getText().toString();

            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(firstname)) {
                edtFirstname.setError(getString(R.string.error_field_required));
                focusView = edtFirstname;
                cancel = true;
            }
            if (TextUtils.isEmpty(lastname)) {
                edtLastname.setError(getString(R.string.error_field_required));
                focusView = edtLastname;
                cancel = true;
            }
            // Check for a valid email address.
//            if (TextUtils.isEmpty(email)) {
//                edtEmail.setError(getString(R.string.error_field_required));
//                focusView = edtEmail;
//                cancel = true;
//            } else if (!isEmailValid(email)) {
//                edtEmail.setError(getString(R.string.error_invalid_email));
//                focusView = edtEmail;
//                cancel = true;
//            }

            if (TextUtils.isEmpty(username)) {
                edtUsername.setError(getString(R.string.error_field_required));
                focusView = edtUsername;
                cancel = true;
            }


            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                progressDialog = ProgressDialog.show(
                        getActivity(), "Update", "Updating profile...", true);
                mAuthTask = new UpdateProfileTask(firstname, lastname, email, username);
                mAuthTask.execute((Void) null);
            }

        } else {
            showAlert("No Internet Connection", "You don't have internet connection.");
        }
    }

    public void showAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // Setting alert dialog icon
        alertDialog.setIcon(R.mipmap.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class UpdateProfileTask extends AsyncTask<Void, Void, Boolean> {
        private final String mFirstname;
        private final String mLastname;
        private final String mEmail;
        private final String mUsername;

        UpdateProfileTask(String firstname, String lastname, String email, String username) {
            mFirstname = firstname;
            mLastname = lastname;
            mEmail = email;
            mUsername = username;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }
            Log.i("getKeyObjectid", sharedPref.getKeyObjectid());
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
            parseQuery.getInBackground(sharedPref.getKeyObjectid(), new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        object.put("Firstname",mFirstname);
                        object.put("Lastname",mLastname);
//                        object.put("email",mEmail);
                        object.put("username",mUsername);
                        object.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e==null){
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Profile successfully updated!", Toast.LENGTH_LONG).show();
//                                    Activity_Main.txtNavEmail.setText(mEmail);
//                                    Activity_Main.txtNavName.setText(mFirstname + " " + mLastname);
                                    sharedPref.setKeyUsername(mUsername);
                                    sharedPref.setKeyName(mFirstname + " " + mLastname);
//                                    sharedPref.setKeyEmail(mEmail);
                                }else{
                                    e.printStackTrace();
//                                        progressDialog.dismiss();
//                                        showAlert("Update Failed",
//                                                e.getMessage().toString());
                                }
                            }
                        });
                    } else {
                        e.printStackTrace();
                    }

                }
            });
//            if (parseUser.getUsername() != null) {
//                parseUser.setUsername(mUsername);
//                parseUser.setEmail(mEmail);
//                parseUser.put("Firstname", mFirstname);
//                parseUser.put("Lastname", mLastname);
//                parseUser.saveInBackground(new SaveCallback() {
//                    @Override
//                    public void done(ParseException e) {
//                        if (e == null) {
//                            progressDialog.dismiss();
//                            Toast.makeText(getActivity(), "Profile successfully updated!", Toast.LENGTH_LONG).show();
//                            sharedPref = new SharedPref(getActivity());
//
//                            parseUser.logOutInBackground(new LogOutCallback() {
//                                @Override
//                                public void done(ParseException e) {
//                                    if (e == null) {
//                                        ParseUser.logInInBackground(mUsername, sharedPref.getKeyPassword(), new LogInCallback() {
//                                            @Override
//                                            public void done(ParseUser user, ParseException e) {
//                                                if (e == null) {
//                                                    Activity_Main.txtNavEmail.setText(user.getEmail());
//                                                    Activity_Main.txtNavName.setText(user.get("Firstname") + " " + user.get("Lastname"));
//                                                    sharedPref.setKeyUsername(mUsername);
//                                                    sharedPref.setKeyName(mFirstname + " " + mLastname);
//                                                    sharedPref.setKeyEmail(mEmail);
//                                                }
//                                            }
//                                        });
//                                    }
//                                }
//                            });
//
//                        } else {
//                            progressDialog.dismiss();
//                            showAlert("Update Failed",
//                                    e.getMessage().toString());
//                        }
//                    }
//                });
//            } else {
//                Log.i("getKeyObjectid",sharedPref.getKeyObjectid());
//                ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
//                parseQuery.getInBackground(sharedPref.getKeyObjectid(), new GetCallback<ParseObject>() {
//                    @Override
//                    public void done(ParseObject object, ParseException e) {
//                        if (e == null) {
//                            object.put("Firstname",mFirstname);
//                            object.put("Lastname",mLastname);
//                            object.put("email",mEmail);
//                            object.put("username",mUsername);
//                            object.saveInBackground(new SaveCallback() {
//                                @Override
//                                public void done(ParseException e) {
//                                    if (e==null){
//                                        progressDialog.dismiss();
//                                        Toast.makeText(getActivity(), "Profile successfully updated!", Toast.LENGTH_LONG).show();
//                                        Activity_Main.txtNavEmail.setText(mEmail);
//                                        Activity_Main.txtNavName.setText(mFirstname + " " + mLastname);
//                                        sharedPref.setKeyUsername(mUsername);
//                                        sharedPref.setKeyName(mFirstname + " " + mLastname);
//                                        sharedPref.setKeyEmail(mEmail);
//                                    }else{
//                                        e.printStackTrace();
////                                        progressDialog.dismiss();
////                                        showAlert("Update Failed",
////                                                e.getMessage().toString());
//                                    }
//                                }
//                            });
//                        } else {
//                            e.printStackTrace();
//                        }
//
//                    }
//                });
//            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPreExecute() {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        showAlert("Connection Timeout.",
                                "Please try again.");
                        ParseUser.getQuery().cancel();
                        mAuthTask = null;
                    }


                }
            }, 60000);
            progressDialog.show();
            super.onPreExecute();
        }

//        @Override
//        protected void onPostExecute(final Boolean success) {
//            mAuthTask = null;
//            progressDialog.dismiss();
//
//            if (success) {
//                finish();
//            } else {
//                edtUsername.setError(getString(R.string.error_incorrect_password));
//                edtPassword.requestFocus();
//            }
//        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            progressDialog.dismiss();
        }
    }


}
