package com.hashmap.tk.criminologyreviewer.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.Model.Result;
import com.hashmap.tk.criminologyreviewer.Utils.Util;

import java.util.ArrayList;
import java.util.List;

public class Datasource {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;


    public Datasource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    /**
     * Open Database
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Close Database
     */
    public void close() {
        dbHelper.close();
    }

    public long addCategory(String title, String set) {
        ContentValues cv = new ContentValues();
        cv.put(dbHelper.COLUMN_TITLE, title);
        cv.put(dbHelper.COLUMN_SET, set);
        ;
        return database.insert(dbHelper.TABLE_CATEGORY, null, cv);
    }

    public void updateScore(int id, float highscore) {
        ContentValues cv = new ContentValues();
        cv.put(dbHelper.COLUMN_HIGHSCORE, highscore);

        database.update(dbHelper.TABLE_CATEGORY, cv, dbHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});

    }

    public boolean hasRows() {
        String selectQuery = "SELECT  * FROM " + dbHelper.TABLE_CATEGORY;

        return database.rawQuery(selectQuery, null).getCount() > 0;
    }

    public void addQuestion(long categoryId, String question, List<String> choices, int answer) {

        ContentValues cv = new ContentValues();
        cv.put(dbHelper.COLUMN_CATEGORY_ID, categoryId);
        cv.put(dbHelper.COLUMN_QUESTION, question);
        cv.put(dbHelper.COLUMN_CHOICES, Util.serialize(choices.toArray(new String[choices.size()])));
        cv.put(dbHelper.COLUMN_ANSWER, answer);

        database.insert(dbHelper.TABLE_QUESTIONS, null, cv);
    }

    public Cursor queryQuestions(long categoryId) {
        String selectQuery = "SELECT  * FROM " + dbHelper.TABLE_QUESTIONS + " WHERE " + dbHelper.COLUMN_CATEGORY_ID + " = " + categoryId;

        return database.rawQuery(selectQuery, null);
    }

    public List<Category> queryCategory() {
        String selectQuery = "SELECT  * FROM " + dbHelper.TABLE_CATEGORY;
        List<Category> categoryList = new ArrayList<>();

        Cursor c = database.rawQuery(selectQuery, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            Category category = new Category();

            category.setId(c.getString(0));
            category.setTitle(c.getString(1));
            category.setSet(c.getString(2));
            categoryList.add(category);
            c.moveToNext();
        }
        c.close();
        return categoryList;
    }

    public String getCategoryTitle(String id) {
        String selectQuery = "SELECT title FROM " + dbHelper.TABLE_CATEGORY+" WHERE id = "+id;
        String title = null;

        Cursor c = database.rawQuery(selectQuery, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            title = c.getString(0);
            c.moveToNext();
        }
        c.close();
        return title;
    }

    public void addModule(List<String> module) {
        ContentValues cv = new ContentValues();
        for (int i = 0; i < module.size(); i++) {
            cv.put(dbHelper.COLUMN_NAME, module.get(i));
            database.insert(dbHelper.TABLE_MODULE, null, cv);

        }
    }

    public void addModuleNote(String value) {
        ContentValues cv = new ContentValues();
        cv.put(dbHelper.COLUMN_NOTE, value);
        database.insert(dbHelper.TABLE_MODULE, null, cv);

    }
    public List<Result>getResult(){
        List<Result> listResult = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + dbHelper.TABLE_RESULT;

        Cursor c = database.rawQuery(selectQuery, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            Result q = new Result();
            q.setQuestion(c.getString(1));
            q.setAnswerWord(c.getString(2));
            q.setYourAnswerWord(c.getString(3));
            listResult.add(q);
            c.moveToNext();
        }
        c.close();

        return listResult;
    }
    public void addResult(String question,String ans,String yourans) {
        ContentValues cv = new ContentValues();
        cv.put(dbHelper.COLUMN_QUESTION, question);
        cv.put(dbHelper.COLUMN_CORRECT_ANS, ans);
        cv.put(dbHelper.COLUMN_YOUR_ANSWER, yourans);
        database.insert(dbHelper.TABLE_RESULT, null, cv);

    }
    public void deleteResult() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(MySQLiteHelper.TABLE_RESULT, null, null);
    }


    public boolean updateModule(String id, String newModule, String isDownloaded) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_NAME, newModule);
        values.put(MySQLiteHelper.COLUMN_ISDOWNLOADED, isDownloaded);
        database.update(MySQLiteHelper.TABLE_MODULE, values, MySQLiteHelper.COLUMN_ID + " = ?", new String[]{id});
        //System.out.println("Updated : " + database.update(MySQLiteHelper.TABLE_TIME_LOGS, values, MySQLiteHelper.COLUMN_ID + " = ?", new String[]{strId}));
        return true;
    }

    public List<String> getTitle() {
        List<String> listModule = new ArrayList<>();
        String selectQuery = "SELECT title FROM " + dbHelper.TABLE_MODULE;

        Cursor c = database.rawQuery(selectQuery, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            listModule.add(c.getString(0));
            c.moveToNext();
        }
        c.close();

        return listModule;
    }

    public String getModuleNote(int id) {
        String moduleNote = null;
        String selectQuery = "SELECT notes FROM " + dbHelper.TABLE_MODULE + " WHERE id = " + id;

        Cursor c = database.rawQuery(selectQuery, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            moduleNote = c.getString(0);
            c.moveToNext();
        }
        c.close();

        return moduleNote;
    }

    public boolean moduleHasRows() {
        String selectQuery = "SELECT  * FROM " + dbHelper.TABLE_MODULE;

        return database.rawQuery(selectQuery, null).getCount() > 0;
    }

    public boolean moduleExist(int id) {
        String selectQuery = "SELECT notes FROM " + dbHelper.TABLE_MODULE + " WHERE id = " + id;

        return database.rawQuery(selectQuery, null).getCount() > 0;
    }

    public Cursor getCategoryDetails(String cat_id) {

        String selectCategoryDetails = "SELECT * FROM " + MySQLiteHelper.CREATE_SQL_CATEGORY + " WHERE " + MySQLiteHelper.COLUMN_ID + " =?";

        return database.rawQuery(selectCategoryDetails, new String[]{cat_id});
    }
}
