package com.hashmap.tk.criminologyreviewer.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Crypto.AES_RIJNDAEL;
import com.hashmap.tk.criminologyreviewer.Model.User;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * A login screen that offers login via email/password.
 */
public class Activity_Login extends AppCompatActivity {


    User userModel = new User();
    ProgressDialog pDialog;
    SharedPref sharedPref;
    private UserLoginTask mAuthTask = null;
    // UI references.
    private EditText edtUsername;
    private EditText edtPassword;
    private TextView txtForgotPassword, txthelp;
    private Button btnSignIn;
    private Button btnRegister;
    private View mLoginFormView;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private AES_RIJNDAEL crypt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ParseUser currentUser = ParseUser.getCurrentUser();
//        System.out.println("User : " + currentUser.getUsername());
        sharedPref = new SharedPref(this);
        if (!sharedPref.getInitialization()) {
            sharedPref.setInitialization(true);
            sharedPref.setTimeLimit("0");
            sharedPref.setAutoNext(false);
            sharedPref.setSound(false);
//                    Toast.makeText(Activity_Login.this, String.valueOf(sharedPref.getInitialization() + " / " + sharedPref.getTimeLimit()), Toast.LENGTH_SHORT).show();
        }
        crypt = new AES_RIJNDAEL();
        try {
            Log.i("Password",crypt.decrypt("42R3+gqBFwoBxqB0AUIf6A==", "$up3ru$3r"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Log.i("getKeyObjectid", sharedPref.getKeyObjectid());
            if (!sharedPref.getKeyObjectid().equals("")) {
                startActivity(new Intent(Activity_Login.this, Activity_Main.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Set up the login form.
        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        txtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        txthelp = (TextView) findViewById(R.id.txtHelp);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        cd = new ConnectionDetector(getApplicationContext());

        btnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_Login.this, Activity_Register.class));
            }
        });
        txtForgotPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_Login.this, Activity_ForgotPassword.class));
            }
        });
        txthelp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_Login.this, Activity_Help.class));
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (mAuthTask != null) {
                return;
            }

            // Reset errors.
            edtUsername.setError(null);
            edtPassword.setError(null);

            // Store values at the time of the login attempt.
            String username = edtUsername.getText().toString();
            String password = edtPassword.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                edtPassword.setError(getString(R.string.error_invalid_password));
                focusView = edtPassword;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(username)) {
                edtUsername.setError(getString(R.string.error_field_required));
                focusView = edtUsername;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true);
                mAuthTask = new UserLoginTask(username, password);
                mAuthTask.execute((Void) null);
            }
        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(Activity_Login.this).create();

            // Setting Dialog Title
            alertDialog.setTitle("No Internet Connection");

            // Setting Dialog Message
            alertDialog.setMessage("You don't have internet connection.");

            // Setting alert dialog icon
            alertDialog.setIcon(R.mipmap.ic_launcher);

            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (show) {
            pDialog = new ProgressDialog(Activity_Login.this);
            pDialog.setIndeterminate(true);
            pDialog.setTitle("Login");
            pDialog.setMessage("Logging in....");
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            pDialog.dismiss();
        }

    }

    @SuppressWarnings("deprecation")
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(Activity_Login.this)
                .create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        //    alertDialog.setIcon(R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        if (!((Activity) context).isFinishing()) {
            // show dialog
            alertDialog.show();
        }

    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mUsername = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

//            try {
//                // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                return false;
//            }

//            ParseUser.logInInBackground(mUsername, mPassword, new LogInCallback() {
//                public void done(ParseUser user, ParseException e) {
//                    if (e == null && String.valueOf(user.get("Active")).equals(true)) {
//                        sharedPref = new SharedPref(Activity_Login.this);
//                        sharedPref.setKeyUsername(mUsername);
//                        sharedPref.setKeyObjectid(user.getObjectId());
//                        sharedPref.setKeyPassword(mPassword);
//                        sharedPref.setKeyName(user.get("Firstname") + " " + user.get("Middle") + " " + user.get("Lastname"));
//                        sharedPref.setKeyEmail(String.valueOf(user.getEmail()));
//                        sharedPref.setKeyType(String.valueOf(user.get("AccessType")));
//                        System.out.println("USER :" + user.get("AccessType"));
//                        System.out.println("USER :" + user.getUsername());
//                        System.out.println("USER :" + user.getEmail());
//                        userModel.setUserObjId(user.getObjectId());
//                        System.out.println("USER :" + user.getObjectId());
//                        System.out.println("USER :" + user.get("Firstname") + " " + user.get("Lastname"));
//                        System.out.println("USER :" + user.get("Name_Of_School"));
//                        startActivity(new Intent(Activity_Login.this, Activity_Main.class));
//                        finish();
//                        mAuthTask = null;
//                        showProgress(false);
//                    } else {
//                        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
//                        parseQuery.whereEqualTo("username", mUsername);
//                        parseQuery.whereEqualTo("Active", true);
//                        try {
//                            parseQuery.whereEqualTo("CryptPass", crypt.encrypt(mPassword, "$up3ru$3r"));
//                        } catch (Exception e1) {
//                            e1.printStackTrace();
//                        }
//                        parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
//                            @Override
//                            public void done(ParseObject object, ParseException e) {
//                                if (e == null) {
//                                    System.out.println("USER :" + object.get("AccessType"));
//                                    System.out.println("USER :" + object.get("username"));
//                                    System.out.println("USER :" + object.get("email"));
//                                    userModel.setUserObjId(object.getObjectId());
//                                    System.out.println("USER :" + object.getObjectId());
//                                    System.out.println("USER :" + object.get("Firstname") + " " + object.get("Lastname"));
//                                    sharedPref = new SharedPref(Activity_Login.this);
//                                    sharedPref.setKeyObjectid(object.getObjectId());
//                                    sharedPref.setKeyUsername(mUsername);
//                                    sharedPref.setKeyName(object.get("Firstname") + " " + object.get("Lastname"));
//                                    sharedPref.setKeyEmail(String.valueOf(object.get("email")));
//                                    sharedPref.setKeyPassword(mPassword);
//                                    sharedPref.setKeyType(String.valueOf(object.get("AccessType")));
//                                    startActivity(new Intent(Activity_Login.this, Activity_Main.class));
//                                    finish();
//                                    mAuthTask = null;
//                                    showProgress(false);
//                                } else {
//                                    e.printStackTrace();
//                                    mAuthTask = null;
//                                    showProgress(false);
//                                    Toast.makeText(Activity_Login.this, "Failed to log in account.", Toast.LENGTH_LONG).show();
//                                }
//                            }
//                        });
//
//
//                    }
//
//                }
//            });
//
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Users");
            parseQuery.whereEqualTo("username", mUsername);
            parseQuery.whereEqualTo("Active", true);
            try {
                parseQuery.whereEqualTo("CryptPass", crypt.encrypt(mPassword, "$up3ru$3r"));
                Log.i("Password",crypt.encrypt("password", "$up3ru$3r"));
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        System.out.println("USER :" + object.get("AccessType"));
                        System.out.println("USER :" + object.get("username"));
                        System.out.println("USER :" + object.get("email"));
                        userModel.setUserObjId(object.getObjectId());
                        System.out.println("USER :" + object.getObjectId());
                        System.out.println("USER :" + object.get("Firstname") + " " + object.get("Lastname"));
                        sharedPref = new SharedPref(Activity_Login.this);
                        sharedPref.setKeyObjectid(object.getObjectId());
                        sharedPref.setKeyUsername(mUsername);
                        sharedPref.setKeyName(object.get("Firstname") + " " + object.get("Lastname"));
//                        sharedPref.setKeyEmail(String.valueOf(object.get("email")));
                        sharedPref.setKeyPassword(mPassword);
                        sharedPref.setKeyType(String.valueOf(object.get("AccessType")));
                        startActivity(new Intent(Activity_Login.this, Activity_Main.class));
                        finish();
                        mAuthTask = null;
                        showProgress(false);
                    } else {
                        e.printStackTrace();
                        mAuthTask = null;
                        showProgress(false);
                        Toast.makeText(Activity_Login.this, "Failed to log in account.", Toast.LENGTH_LONG).show();
                    }
                }
            });

            return true;
        }

        @Override
        protected void onPreExecute() {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                        showAlertDialog(Activity_Login.this,
                                "Connection Timeout.",
                                "Please try again.", false);
                        ParseUser.getQuery().cancel();
                        mAuthTask = null;
                        showProgress(false);
                    }


                }
            }, 60000);
            super.onPreExecute();
        }

//        @Override
//        protected void onPostExecute(final Boolean success) {
//            mAuthTask = null;
//            showProgress(false);
//
//            if (success) {
//                startActivity(new Intent(Activity_Login.this,Activity_Main.class));
//                finish();
//                mAuthTask = null;
//                showProgress(false);
//            } else {
//                edtUsername.setError(getString(R.string.error_incorrect_password));
//                edtPassword.requestFocus();
//            }
//        }
//
//        @Override
//        protected void onCancelled() {
//            mAuthTask = null;
//            showProgress(false);
//        }
    }


}

