package com.hashmap.tk.criminologyreviewer.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class Activity_Register extends AppCompatActivity {
    EditText edtFirstname,
            edtLastname,
            edtNOS,
            edtEmail,
            edtUsername,
            edtPassword,
            edtCPassword,
            edtIDNumber;
    Button btnCreateAccount;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private CreateUserTask mAuthTask;
    private Dialog progressDialog;
    private boolean isNotYetRegisterd;
    private String autObjecitID;
    private ProgressDialog pDialog;
    private String firstname;
    private String nos;
    private String username;
    private String cpassword;
    private String password;
    private String lastname;
    private String email;
//    private String idnumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getApplicationContext());
        setContentView(R.layout.activity_register);
        edtFirstname = (EditText) findViewById(R.id.edtFirstname);
        edtLastname = (EditText) findViewById(R.id.edtLastname);
        edtNOS = (EditText) findViewById(R.id.edtNOS);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtCPassword = (EditText) findViewById(R.id.edtCPassword);
//        edtIDNumber = (EditText) findViewById(R.id.edtIDNumber);
        btnCreateAccount = (Button) findViewById(R.id.btnCreateAccount);

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateAccount();
            }
        });
    }

    public void CreateAccount() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            // Reset errors.
            edtFirstname.setError(null);
            edtLastname.setError(null);
            edtNOS.setError(null);
            edtEmail.setError(null);
            edtUsername.setError(null);
            edtPassword.setError(null);
            edtCPassword.setError(null);
//            edtIDNumber.setError(null);

            // Store values at the time of the login attempt.
            firstname = edtFirstname.getText().toString();
            lastname = edtLastname.getText().toString();
//            nos = edtNOS.getText().toString();
            email = edtEmail.getText().toString();
            username = edtUsername.getText().toString();
            password = edtPassword.getText().toString();
            cpassword = edtCPassword.getText().toString();
//            idnumber = edtIDNumber.getText().toString();

            boolean cancel = false;
            View focusView = null;

            if (TextUtils.isEmpty(firstname)) {
                edtFirstname.setError(getString(R.string.error_field_required));
                focusView = edtFirstname;
                cancel = true;
            }
            if (TextUtils.isEmpty(lastname)) {
                edtLastname.setError(getString(R.string.error_field_required));
                focusView = edtLastname;
                cancel = true;
            }
//            if (TextUtils.isEmpty(nos)) {
//                edtNOS.setError(getString(R.string.error_field_required));
//                focusView = edtNOS;
//                cancel = true;
//            }
            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                edtEmail.setError(getString(R.string.error_field_required));
                focusView = edtEmail;
                cancel = true;
            } else if (!isEmailValid(email)) {
                edtEmail.setError(getString(R.string.error_invalid_email));
                focusView = edtEmail;
                cancel = true;
            }

            if (TextUtils.isEmpty(username)) {
                edtUsername.setError(getString(R.string.error_field_required));
                focusView = edtUsername;
                cancel = true;
            }

            // Check for a valid password, if the user entered one.
            if (TextUtils.isEmpty(password)) {
                edtPassword.setError(getString(R.string.error_invalid_password));
                focusView = edtPassword;
                cancel = true;
            } else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                edtPassword.setError(getString(R.string.error_invalid_password));
                focusView = edtPassword;
                cancel = true;
            }
            // Check for a valid password, if the user entered one.
            if (TextUtils.isEmpty(password)) {
                edtCPassword.setError(getString(R.string.error_invalid_password));
                focusView = edtCPassword;
                cancel = true;
            } else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                edtCPassword.setError(getString(R.string.error_invalid_password));
                focusView = edtCPassword;
                cancel = true;
            } else if (!cpassword.equals(password)) {
                edtCPassword.setError(getString(R.string.error_password_not_match));
                focusView = edtCPassword;
                cancel = true;
            }
//            if (idnumber.equals(null) || idnumber.equals("")) {
//                edtIDNumber.setError(getString(R.string.error_field_required));
//                focusView = edtIDNumber;
//                cancel = true;
//            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
//                isAccountValid(idnumber);
                Activity_Register.this.progressDialog = ProgressDialog.show(
                        Activity_Register.this, "Sign up", "Creating account...", true);
                mAuthTask = new CreateUserTask(firstname, lastname, email, username, password);
                mAuthTask.execute((Void) null);
            }

        } else {
            showAlert("No Internet Connection", "You don't have internet connection.");
        }
    }

    public void showAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(Activity_Register.this).create();
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // Setting alert dialog icon
        alertDialog.setIcon(R.mipmap.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void isAccountValid(final String IDNumber) {
        pDialog = new ProgressDialog(Activity_Register.this);
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(true);
        pDialog.setTitle("Account Verification");
        pDialog.setMessage("Authenticating account...");
        pDialog.show();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Authentication");
        query.whereEqualTo("IDNumber", IDNumber);
//        query.whereNotEqualTo("valid", true);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                boolean isRegistered = object.getBoolean("valid");
                firstname = object.get("Firstname").toString();
                lastname = object.get("Lastname").toString();
                if (e == null) {
                    if (isRegistered){
                        pDialog.dismiss();
                        showAlert("Registration Failed!", "Sorry the account number " +IDNumber+ " is already registered.\n\n Please sign in your account.");
                    }else {
                        pDialog.dismiss();
                        autObjecitID = object.getObjectId().toString();
                        Activity_Register.this.progressDialog = ProgressDialog.show(
                                Activity_Register.this, "Sign up", "Creating account...", true);
                        mAuthTask = new CreateUserTask(firstname, lastname, email, username, password);
                        mAuthTask.execute((Void) null);
                    }

                } else {
                    e.printStackTrace();
                    pDialog.dismiss();
                    showAlert("Authentication Failed!", "Sorry you don't have permission to access this application.\n\n Please contact or visit the Dean of College of Crimimology" +
                            " to give you authorization.");
                }
            }
        });
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    public class CreateUserTask extends AsyncTask<Void, Void, Boolean> {
        private final String mFirstname;
        private final String mLastname;
//        private final String mNOS;
        private final String mEmail;
        private final String mUsername;
        private final String mPassword;

        CreateUserTask(String firstname, String lastname, String email, String username, String password) {
            mFirstname = firstname;
            mLastname = lastname;
//            mNOS = nos;
            mEmail = email;
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }
            ParseUser user = new ParseUser();

            user.put("Firstname", mFirstname);
            user.put("Lastname", mLastname);
//            user.put("Name_Of_School", mNOS);
            user.setEmail(mEmail);
            user.setUsername(mUsername);
            user.setPassword(mPassword);

            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        SharedPref sharedPref = new SharedPref(Activity_Register.this);
                        sharedPref.setKeyPassword(mPassword);
                        Toast.makeText(Activity_Register.this, "Successfully registered!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(Activity_Register.this, Activity_Main.class));
                        finish();
                    } else {
                        progressDialog.dismiss();
                        System.out.println("ERROR in SIGN UP - "+e.getMessage().toString());
                        Toast.makeText(Activity_Register.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }



//                    if (e == null) {
//                        ParseUser.logInInBackground(mUsername, mPassword, new LogInCallback() {
//                            @Override
//                            public void done(final ParseUser user, ParseException e) {
//                                if (e == null) {
//                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Authentication");
//                                    Log.i("autObjecitID",autObjecitID);
//                                    query.getInBackground(autObjecitID, new GetCallback<ParseObject>() {
//                                        @Override
//                                        public void done(ParseObject object, ParseException e) {
//                                            if (e == null) {
//                                                Log.i("autObjecitID",object.getObjectId());
//
//                                                object.put("valid", true);
//                                                object.put("Owner", ParseObject.createWithoutData("_User", user.getObjectId()));
//                                                object.saveInBackground(new SaveCallback() {
//                                                    @Override
//                                                    public void done(ParseException e) {
//                                                        Log.i("autObjecitID","done");
//
//                                                        if (e == null) {
//                                                            if (progressDialog != null) {
//                                                                progressDialog.dismiss();
//                                                                Toast.makeText(Activity_Register.this, "Successfully registered!", Toast.LENGTH_LONG).show();
//                                                                startActivity(new Intent(Activity_Register.this, Activity_Main.class));
//                                                                finish();
//                                                            } else {
//                                                                Toast.makeText(Activity_Register.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
//
//                                                            }
//                                                        }
//                                                    }
//                                                });
//                                            } else {
//                                                progressDialog.dismiss();
//                                                Toast.makeText(Activity_Register.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
//
//                                            }
//
//                                        }
//                                    });
//                                } else {
//
//                                }
//                            }
//                        });
//                    } else {
//                        progressDialog.dismiss();
//                        System.out.println("ERROR in SIGN UP - "+e.getMessage().toString());
//                        Toast.makeText(Activity_Register.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
//
//                    }
                }
            });

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPreExecute() {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        showAlert("Connection Timeout.",
                                "Please try again.");
                        ParseUser.getQuery().cancel();
                        ParseQuery.getQuery("Authentication").cancel();
                        mAuthTask = null;
                    }


                }
            }, 60000);
            progressDialog.show();
            super.onPreExecute();
        }

//        @Override
//        protected void onPostExecute(final Boolean success) {
//            mAuthTask = null;
//            progressDialog.dismiss();
//
//            if (success) {
//                finish();
//            } else {
//                edtUsername.setError(getString(R.string.error_incorrect_password));
//                edtPassword.requestFocus();
//            }
//        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            progressDialog.dismiss();
        }
    }

}
