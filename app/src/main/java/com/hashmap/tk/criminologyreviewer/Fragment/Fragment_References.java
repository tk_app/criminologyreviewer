package com.hashmap.tk.criminologyreviewer.Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.Activity.Activity_ViewModule;
import com.hashmap.tk.criminologyreviewer.Database.Datasource;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Fragment_References extends Fragment implements View.OnClickListener {
    LinearLayout linearDownload, linearReferences;
    ListView listViewReferences;
    ArrayAdapter<String> adapterReferences;
    List<String> listDownload, listReferences;
    Datasource datasource;
    Button btnDownload, btnReferences;
    private ConnectionDetector cd;
    private boolean isInternetPresent;
    private DownloadManager dm;
    private long downloadReference;
    private File direct;
    private File f;
    private String[] reference;
    private String[] url;
    private String[] title;
    private String[] download;
    private String Url, Title;
    private SwingRightInAnimationAdapter mAnimAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_references, container, false);

        cd = new ConnectionDetector(getActivity());
        BroadCastReciever();

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Tap the list item to download reference.", Snackbar.LENGTH_LONG)
                        .show();
            }
        });

        datasource = new Datasource(getActivity());
        datasource.open();
        linearReferences = (LinearLayout) view.findViewById(R.id.layoutReferences);
        listViewReferences = (ListView) view.findViewById(R.id.listViewReferences);

        download = getResources().getStringArray(R.array.download_module);
        reference = getResources().getStringArray(R.array.category_module);
        url = getResources().getStringArray(R.array.module_url);
        title = getResources().getStringArray(R.array.module_title);
//        listReferences = Arrays.asList(reference);
        listDownload = new ArrayList<>();
        for (int i = 0; i < title.length; i++) {
            if (isModuleExist(title[i])) {
                listDownload.add(reference[i]);
            } else {
                listDownload.add(download[i]);
            }

        }


        adapterReferences = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, listDownload);
//        listViewReferences.setAdapter(adapterReferences);

        if (!(mAnimAdapter instanceof SwingRightInAnimationAdapter)) {
            mAnimAdapter = new SwingRightInAnimationAdapter(adapterReferences);
            mAnimAdapter.setAbsListView(listViewReferences);
            listViewReferences.setAdapter(mAnimAdapter);
        }

        listViewReferences.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!isModuleExist(title[position])) {
                    downloadModule(url[position], title[position]);
                    Url = url[position];
                    Title = title[position];
                } else {
                    Intent i = new Intent(getActivity(), Activity_ViewModule.class);
                    i.putExtra("notes", (CharSequence) viewModule(title[position]));
                    startActivity(i);
                }
            }
        });


        return view;
    }

    public void downloadModule(String url, String title) {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            String root = Environment.getExternalStorageDirectory().toString();

//            direct.mkdirs();

            if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                direct = new File(Environment.getExternalStorageDirectory()
                        + "/CriminologyReviewer");
                f = new File(root + "/CriminologyReviewer/" + title);
                if (f.exists()) {
                    direct.mkdirs();
                    Toast t = Toast.makeText(getActivity(), "Replace the file!", Toast.LENGTH_LONG);
                    t.show();
                    t.setGravity(Gravity.CENTER, 0, 0);
                    f.delete();
                } else {
                    Toast t = Toast.makeText(getActivity(), "Starting download!", Toast.LENGTH_LONG);
                    t.show();
                    t.setGravity(Gravity.CENTER, 0, 0);
                }

                dm = (DownloadManager) getActivity().getSystemService(getActivity().DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(url);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(title);
                //Set a description of this download, to be displayed in notifications (if enabled)
                request.setDescription("Downloading file");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalPublicDir("/CriminologyReviewer", title);

//            File file = new File(getActivity().getFilesDir(), title);
//
//            String fileName = "MyFile";
//            String content = "hello world";
//
//            FileOutputStream outputStream = null;
//            try {
//                outputStream = getActivity().openFileOutput(fileName, Context.MODE_PRIVATE);
//                outputStream.write(request.getBytes());
//                outputStream.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

                //Enqueue a new download and same the referenceId
                downloadReference = dm.enqueue(request);
            } else {
                requestPermission(getActivity());
            }


        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

            // Setting Dialog Title
            alertDialog.setTitle("No Internet Connection");

            // Setting Dialog Message
            alertDialog.setMessage("You don't have internet connection.");

            // Setting alert dialog icon
            alertDialog.setIcon(R.mipmap.ic_launcher);

            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }

    }

    public boolean isModuleExist(String title) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/CriminologyReviewer");
        File file = new File(dir, title);
        // Launching new Activity on selecting single List Item
        if (file.exists()) {
            //Read text from file
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
            } catch (IOException e) {
                //You'll need to add proper error handling here
            }

            return true;
        } else {
            return false;
        }
    }

    public StringBuilder viewModule(String title) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/CriminologyReviewer");
        File file = new File(dir, title);
        // Launching new Activity on selecting single List Item
        StringBuilder text = null;
        if (file.exists()) {
            //Read text from file
            text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
            } catch (IOException e) {
                //You'll need to add proper error handling here
            }

        }
        return text;

    }

    public void BroadCastReciever() {
        if (isAdded()) {

            try {
                BroadcastReceiver receiver = new BroadcastReceiver() {
                    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String action = intent.getAction();
                        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                            long downloadId = intent.getLongExtra(
                                    DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                            DownloadManager.Query query = new DownloadManager.Query();
                            query.setFilterById(downloadReference);
                            Cursor c = dm.query(query);
                            if (c.moveToFirst()) {
                                int columnIndex = c
                                        .getColumnIndex(DownloadManager.COLUMN_STATUS);
                                if (DownloadManager.STATUS_SUCCESSFUL == c
                                        .getInt(columnIndex)) {
                                    listReferences = Arrays.asList(reference);
                                    listDownload = new ArrayList<>();
                                    for (int i = 0; i < title.length; i++) {
                                        if (isModuleExist(title[i])) {
                                            listDownload.add(listReferences.get(i));
                                        } else {
                                            listDownload.add(download[i]);
                                        }

                                    }
                                    adapterReferences = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, listDownload);
//                                    adapterReferences.notifyDataSetChanged();
                                    listViewReferences.setAdapter(adapterReferences);

                                    String uriString = c
                                            .getString(c
                                                    .getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                                    Toast t = Toast.makeText(getActivity(), "Download Complete!", Toast.LENGTH_LONG);
                                    t.setGravity(Gravity.CENTER, 0, 0);
                                    t.show();
                                }
                            }
                        }
                    }
                };

                getActivity().registerReceiver(receiver, new IntentFilter(
                        DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.i("onDetach", "Fragment References is attached");
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        Log.i("onDetach", "Fragment References is detached");
        super.onDetach();
    }

    private void requestPermission(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.

            new AlertDialog.Builder(context)
                    .setMessage("Write Storage")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    100);
                            downloadModule(Url, Title);
                        }
                    }).show();

        } else {
            // permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    100);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
