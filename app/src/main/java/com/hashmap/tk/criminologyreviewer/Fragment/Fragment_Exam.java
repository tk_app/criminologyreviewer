package com.hashmap.tk.criminologyreviewer.Fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hashmap.tk.criminologyreviewer.Adapter.Adapter_Fragment_Exam;
import com.hashmap.tk.criminologyreviewer.Adapter.Adapter_Fragment_Exam_2;
import com.hashmap.tk.criminologyreviewer.Database.Datasource;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class Fragment_Exam extends Fragment {

    Datasource datasource;
    List<Category> categoryList;
    Category category;
    ListView listView;
    boolean isLoaded = false;
    private int mColumnCount = 1;
    private List<ParseObject> ob;
    private SwingRightInAnimationAdapter mAnimAdapter;
    private Adapter_Fragment_Exam_2 adapter;
    private RecyclerView recyclerView;
    private SharedPref sharedPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
        datasource = new Datasource(getActivity());
        datasource.open();
        listView = (ListView) view.findViewById(R.id.list);
        sharedPref = new SharedPref(getActivity());
        if (!isLoaded) {
            categoryList = new ArrayList<>();
            categoryList = datasource.queryCategory();
            adapter = new Adapter_Fragment_Exam_2(getActivity(), categoryList, "exam Activity");

            // Set the adapter
            if (view instanceof RecyclerView) {
                Context context = view.getContext();
                recyclerView = (RecyclerView) view;
                if (mColumnCount <= 1) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                } else {
                    recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                }
                recyclerView.setAdapter(new Adapter_Fragment_Exam(getActivity(), categoryList));

            }


            if (!(mAnimAdapter instanceof SwingRightInAnimationAdapter)) {
                mAnimAdapter = new SwingRightInAnimationAdapter(adapter);
                mAnimAdapter.setAbsListView(listView);
                listView.setAdapter(mAnimAdapter);
            }

            getScore();
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void getScore() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {


                ParseUser curreUser = ParseUser.getCurrentUser();
                ParseQuery<ParseObject> query = ParseQuery.getQuery("ScoreBoard");
                query.orderByAscending("category_id");
                query.whereEqualTo("owner", sharedPref.getKeyObjectid());
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if (e == null) {
                            for (int i = 0; i < categoryList.size(); i++) {
                                for (int ii = 0; ii < objects.size(); ii++) {
                                    String o = objects.get(ii).get("category_id").toString();
                                    if (categoryList.get(i).getId().equals(o)) {
                                        categoryList.get(i).setHighscore(objects.get(ii).get("highscore").toString());
                                        categoryList.get(i).setTimelimit(objects.get(ii).get("time").toString());
                                    }
                                }

                                adapter = new Adapter_Fragment_Exam_2(getActivity(), categoryList, "examActivity");
//                                listView.setAdapter(adapter);
                                listView.invalidateViews();
//
//                if (isLoaded) {
                                if (!(mAnimAdapter instanceof SwingRightInAnimationAdapter)) {
                                    mAnimAdapter = new SwingRightInAnimationAdapter(adapter);
                                    mAnimAdapter.setAbsListView(listView);
                                    listView.setAdapter(mAnimAdapter);
                                    mAnimAdapter.notifyDataSetChanged();

                                }
//                }
                                isLoaded = true;
                            }
                        } else {
                            e.printStackTrace();
                        }
                    }
                });


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    @Override
    public void onResume() {
        if (isLoaded) {
            getScore();
        }
        super.onResume();
    }
}
