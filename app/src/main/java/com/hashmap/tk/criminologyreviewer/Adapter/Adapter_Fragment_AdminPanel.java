package com.hashmap.tk.criminologyreviewer.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.hashmap.tk.criminologyreviewer.Activity.Activity_User;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_Admin;
import com.hashmap.tk.criminologyreviewer.Model.Student;
import com.hashmap.tk.criminologyreviewer.R;

import java.util.ArrayList;
import java.util.List;

public class Adapter_Fragment_AdminPanel extends BaseAdapter {
    Context context;
    List<Student> studentList;
    int counter = 0;
    int pos;
    boolean markAll = false;
    boolean isEdit = false;
    boolean isDelete = false;
    List<Integer> integerList;
    private LayoutInflater inflater;
    Fragment_Admin fa;
    public Adapter_Fragment_AdminPanel(Context context, List<Student> studentList) {
        this.context = context;
        this.studentList = studentList;
        inflater = LayoutInflater.from(this.context);
        integerList = new ArrayList<>();
        fa = new Fragment_Admin();
    }

    @Override
    public int getCount() {
        return studentList.size();
    }

    @Override
    public Object getItem(int position) {
        return studentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        this.pos = position;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_admin_list_item, null);
            holder = new ViewHolder();
            holder.txtFullname = (TextView) convertView.findViewById(R.id.txtFullname);
            holder.txtUsername = (TextView) convertView.findViewById(R.id.txtUsername);
            holder.txtSY = (TextView) convertView.findViewById(R.id.txtAcademicYear);
            holder.chkCheckItem = (CheckBox) convertView.findViewById(R.id.chkCheckItem);
            convertView.setTag(holder);
            holder.chkCheckItem.setTag(position);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtFullname.setText(this.studentList.get(position).getStudFirstname() + " " +
                this.studentList.get(position).getStudLastname());
        holder.txtUsername.setText(this.studentList.get(position).getStudUsername());
        holder.txtSY.setText(this.studentList.get(position).getStudSY1() + " - " + this.studentList.get(position).getStudSY2());
        holder.chkCheckItem.setChecked(this.studentList.get(position).isChecked);
//        holder.chkCheckItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                studentList.get(position).isChecked = isChecked;
//            }
//        });
        holder.chkCheckItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (((CheckBox) v).isChecked()) {
                        pos = position;
                        integerList.add(position);
//                        Fragment_Admin.tempparseUserList.add(Fragment_Admin.parseUserList.get(position));
//
                        fa.addObject(Fragment_Admin.parseUserList.get(position).getObjectId(),Fragment_Admin.parseUserList.get(position));
//                        Log.i("Position - add", String.valueOf(integerList));
                        Log.i("Position - add", String.valueOf(fa.getObjects()));
                        counter++;
                        studentList.get(position).isChecked = true;
                    } else {
//                        Fragment_Admin.tempparseUserList.remove(Fragment_Admin.tempparseUserList.indexOf(Fragment_Admin.parseUserList.get(position)));
                        fa.deleteObject(Fragment_Admin.parseUserList.get(position).getObjectId());
                        integerList.remove(integerList.indexOf(position));
//                        Log.i("Position - remove", String.valueOf(integerList));
                        Log.i("Position - remove", String.valueOf(fa.getObjects()));
                        counter--;
                        markAll = false;
                        studentList.get(position).isChecked = false;
                    }
                    if (counter > 1) {
                        isEdit = false;
                        isDelete = true;
                    } else if (counter == 1) {
                        isEdit = true;
                        isDelete = true;
                    } else {
                        isEdit = false;
                        isDelete = false;
                    }
                    Fragment_Admin.txtEdit.setEnabled(isEdit);
                    Fragment_Admin.txtDelete.setEnabled(isDelete);

                    if (counter == studentList.size()) {
                        markAll = true;
                    }
                    if (markAll) {
                        Fragment_Admin.chkAll.setChecked(true);
                    } else {
                        Fragment_Admin.chkAll.setChecked(false);
                    }
                    Log.i(studentList.get(position).getStudUsername(), String.valueOf(studentList.get(position).isChecked));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Student stud = studentList.get(position);
//                Intent intent = new Intent(context, Activity_User.class);
//                intent.putExtra("objectid", stud.getStudObjectID().toString());
//                intent.putExtra("username", stud.getStudUsername().toString());
//                intent.putExtra("lastname", stud.getStudLastname().toString());
//                intent.putExtra("firstname", stud.getStudFirstname().toString());
//                intent.putExtra("middle", stud.getStudMiddle().toString());
//                intent.putExtra("birthdate", new SimpleDateFormat("MMM dd, yyyy").format(stud.getStudBirthdate()));
//                intent.putExtra("section", stud.getStudSection().toString());
//                intent.putExtra("email", stud.getStudEmail().toString());
//                intent.putExtra("sy1", stud.getStudSY1().toString());
//                intent.putExtra("sy2", stud.getStudSY2().toString());
//                intent.putExtra("tag", "update");
//                context.startActivity(intent);
//
//            }
//        });
        return convertView;
    }

    public void EditUSer() {
        Student stud = studentList.get(integerList.get(0));
        Log.i("Position - remain", String.valueOf(integerList));
        Log.i("position", String.valueOf(integerList.get(0)));
        Intent intent = new Intent(context, Activity_User.class);
        intent.putExtra("objectid", stud.getStudObjectID().toString());
        intent.putExtra("username", stud.getStudUsername().toString());
        intent.putExtra("password", stud.getStudStudPassword().toString());
        intent.putExtra("lastname", stud.getStudLastname().toString());
        intent.putExtra("firstname", stud.getStudFirstname().toString());
//        intent.putExtra("middle", stud.getStudMiddle().toString());
//        intent.putExtra("birthdate", new SimpleDateFormat("MMM dd, yyyy").format(stud.getStudBirthdate()));
//        intent.putExtra("section", stud.getStudSection().toString());
//        intent.putExtra("email", stud.getStudEmail().toString());
//        intent.putExtra("sy1", stud.getStudSY1().toString());
//        intent.putExtra("sy2", stud.getStudSY2().toString());
        intent.putExtra("tag", "update");
        context.startActivity(intent);
    }

    public void checkAll(boolean checkAll) {

        for (int i = 0; i < studentList.size(); i++) {
            studentList.get(i).isChecked = checkAll;
        }
        if (checkAll) {
            Fragment_Admin.tempparseUserList.clear();
            fa.map.clear();
            for (int i = 0; i < studentList.size(); i++) {
                integerList.add(i);
//                Fragment_Admin.tempparseUserList.add(Fragment_Admin.parseUserList.get(i));
                fa.addObject(Fragment_Admin.parseUserList.get(i).getObjectId(),Fragment_Admin.parseUserList.get(i));
                Log.i("Position - addAll", String.valueOf(fa.getObjects()));

            }
            markAll = true;
            isDelete = true;
            isEdit = false;
            counter = studentList.size();
        } else {
//            Fragment_Admin.tempparseUserList.clear();
            fa.map.clear();
            integerList.clear();
            Log.i("Position - removeAll", String.valueOf(fa.getObjects()));
            isDelete = false;
            isEdit = false;
            markAll = false;
            counter = 0;

        }
        Fragment_Admin.txtEdit.setEnabled(isEdit);
        Fragment_Admin.txtDelete.setEnabled(isDelete);
        if (markAll) {
            Fragment_Admin.chkAll.setChecked(true);
        } else {
            Fragment_Admin.chkAll.setChecked(false);
        }
        notifyDataSetChanged();

    }



    class ViewHolder {
        TextView txtFullname, txtUsername, txtSY;
        CheckBox chkCheckItem;
    }
}