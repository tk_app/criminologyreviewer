package com.hashmap.tk.criminologyreviewer.Model;

import java.util.Date;

public class Student {
    public String studObjectID;
    public String studStudPassword;
    public String studUsername;
    public String studFirstname;
    public String studLastname;
    public String studMiddle;
    public Date studBirthdate;
    public String studSection;
    public String studSY1;
    public String studSY2;
    public String studEmail;
    public boolean isChecked = false;

    public String getStudObjectID() {
        return studObjectID;
    }

    public void setStudObjectID(String studObjectID) {
        this.studObjectID = studObjectID;
    }

    public String getStudStudPassword() {
        return studStudPassword;
    }

    public void setStudStudPassword(String studStudPassword) {
        this.studStudPassword = studStudPassword;
    }

    public String getStudUsername() {
        return studUsername;
    }

    public void setStudUsername(String studUsername) {
        this.studUsername = studUsername;
    }

    public String getStudFirstname() {
        return studFirstname;
    }

    public void setStudFirstname(String studFirstname) {
        this.studFirstname = studFirstname;
    }

    public String getStudLastname() {
        return studLastname;
    }

    public void setStudLastname(String studLastname) {
        this.studLastname = studLastname;
    }

    public String getStudMiddle() {
        return studMiddle;
    }

    public void setStudMiddle(String studMiddle) {
        this.studMiddle = studMiddle;
    }

    public Date getStudBirthdate() {
        return studBirthdate;
    }

    public void setStudBirthdate(Date studBirthdate) {
        this.studBirthdate = studBirthdate;
    }

    public String getStudSection() {
        return studSection;
    }

    public void setStudSection(String studSection) {
        this.studSection = studSection;
    }

    public String getStudSY1() {
        return studSY1;
    }

    public void setStudSY1(String studSY1) {
        this.studSY1 = studSY1;
    }

    public String getStudSY2() {
        return studSY2;
    }

    public void setStudSY2(String studSY2) {
        this.studSY2 = studSY2;
    }

    public String getStudEmail() {
        return studEmail;
    }

    public void setStudEmail(String studEmail) {
        this.studEmail = studEmail;
    }
}
