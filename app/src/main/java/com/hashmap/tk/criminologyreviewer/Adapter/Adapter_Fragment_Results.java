package com.hashmap.tk.criminologyreviewer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.R;

import java.util.List;

public class Adapter_Fragment_Results extends BaseAdapter {

    List<Category> _categoryList = null;
    Context _context;
    static String _indicator = null;

    public Adapter_Fragment_Results(Context context, List<Category> categoryList, String indicator) {
        this._categoryList = categoryList;
        this._context = context;
        this._indicator = indicator;
    }

    @Override
    public int getCount() {
        return _categoryList.size();
    }

    @Override
    public Object getItem(int pos) {
        return pos;
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        ViewHolder holder = null;
        convertView = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_item, parent, false);

            holder = new ViewHolder();
            holder.txtScore = (TextView) convertView.findViewById(R.id.txtPercent);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtSet);

            convertView.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        if (_indicator.equals("category")) {
            holder.txtTitle.setText(_categoryList.get(position).getTitle());
            holder.txtDate.setText(_categoryList.get(position).getDate());
//            Log.i("CategoryList", _categoryList.get(position).getTitle());
        } else {
            int takes = (position + 1);
            holder.txtTitle.setText("Take #" + takes);
            holder.txtDate.setText(_categoryList.get(position).getDate());
            holder.txtScore.setText(_categoryList.get(position).getScore() + "%");
        }

        return convertView;
    }

    class ViewHolder {
        TextView txtScore, txtTitle, txtDate;
    }
}
