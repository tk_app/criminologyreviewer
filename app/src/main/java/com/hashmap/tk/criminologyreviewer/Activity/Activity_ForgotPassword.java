package com.hashmap.tk.criminologyreviewer.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.ConnectionDetector;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class Activity_ForgotPassword extends AppCompatActivity {
    EditText et_forgetpassword = null;
    Button btn_submitforgetpassword = null;
    String password = null;
    ProgressDialog pDialog;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);

        et_forgetpassword = (EditText) findViewById(R.id.et_forgetpassword);
        btn_submitforgetpassword = (Button) findViewById(R.id.btn_submitforgetpassword);
        cd = new ConnectionDetector(getApplicationContext());
        btn_submitforgetpassword.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                password = et_forgetpassword.getText().toString();
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    checkEmailID();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(Activity_ForgotPassword.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("No Internet Connection");

                    // Setting Dialog Message
                    alertDialog.setMessage("You don't have internet connection.");

                    // Setting alert dialog icon
                    alertDialog.setIcon(R.mipmap.ic_launcher);

                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }


            }
        });

    }

    protected void checkEmailID() {
        // TODO Auto-generated method stub
        if (TextUtils.isEmpty(password)) {
            et_forgetpassword.setError(getString(R.string.error_field_required));
        } else if (!password.contains("@")) {
            et_forgetpassword.setError(getString(R.string.error_invalid_email));
        } else {
            showProgress(true);
            forgotPassword(password);
        }
    }

    public void forgotPassword(String email) {
        //postEvent(new UserForgotPasswordStartEvent());
        ParseUser.requestPasswordResetInBackground(email, new UserForgotPasswordCallback());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (show) {
            pDialog = new ProgressDialog(Activity_ForgotPassword.this);
            pDialog.setIndeterminate(true);
            pDialog.setTitle("Reset Password");
            pDialog.setMessage("Validating email....");
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            pDialog.dismiss();
        }

    }

    private class UserForgotPasswordCallback implements RequestPasswordResetCallback {
        public UserForgotPasswordCallback() {
            super();
        }

        @Override
        public void done(ParseException e) {
            if (e == null) {
                et_forgetpassword.setText("");
                showProgress(false);
                Toast.makeText(getApplicationContext(), "Successfully sent link to your email for reset Password", Toast.LENGTH_LONG).show();
            } else {
                showProgress(false);
                Toast.makeText(getApplicationContext(), "Failed to sent link to your email for reset Password", Toast.LENGTH_LONG).show();

            }
        }
    }
}
