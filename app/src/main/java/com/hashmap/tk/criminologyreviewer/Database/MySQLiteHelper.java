package com.hashmap.tk.criminologyreviewer.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {
    // Database Name
    public static String DATABASE_NAME = "quiz.db";

    // Current version of database
    private static final int DATABASE_VERSION = 1;

    // Name of table
    public static final String TABLE_CATEGORY = "Category";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_SET = "text";
    public static final String COLUMN_HIGHSCORE = "highscore";

    public static final String TABLE_QUESTIONS = "Questions";
    public static final String COLUMN_CATEGORY_ID = "category_id";
    public static final String COLUMN_QUESTION = "question";
    public static final String COLUMN_CHOICES = "choices";
    public static final String COLUMN_ANSWER = "answer";

    public static final String TABLE_MODULE = "Module";
    public static final String COLUMN_NAME = "title";
    public static final String COLUMN_ISDOWNLOADED = "isdownloaded";
    public static final String COLUMN_NOTE = "notes";


    public static final String TABLE_RESULT = "Result";
    public static final String COLUMN_YOUR_ANSWER = "answer";
    public static final String COLUMN_CORRECT_ANS = "youranswer";

    // All Keys used in table
    public static final String COLUMN_ID = "id";

    public static final String CREATE_SQL_CATEGORY =
            "CREATE TABLE " + TABLE_CATEGORY + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_TITLE + " TEXT,"
                    + COLUMN_SET + " TEXT,"
                    + COLUMN_HIGHSCORE + " REAL )";

    public static final String CREATE_SQL_QUESTION =
            "CREATE TABLE " + TABLE_QUESTIONS + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_CATEGORY_ID + " INTEGER,"
                    + COLUMN_QUESTION + " TEXT,"
                    + COLUMN_CHOICES + " BLOB,"
                    + COLUMN_ANSWER + " INTEGER)";

    public static final String CREATE_SQL_MODULE =
            "CREATE TABLE " + TABLE_MODULE + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT, "
                    + COLUMN_ISDOWNLOADED + " TEXT, "
                    + COLUMN_NOTE + " TEXT)";

    public static final String CREATE_SQL_RESULT =
            "CREATE TABLE " + TABLE_RESULT + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_QUESTION + " TEXT, "
                    + COLUMN_CORRECT_ANS + " TEXT, "
                    + COLUMN_YOUR_ANSWER + " TEXT)";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SQL_CATEGORY);
        db.execSQL(CREATE_SQL_QUESTION);
        db.execSQL(CREATE_SQL_MODULE);
        db.execSQL(CREATE_SQL_RESULT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODULE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULT);
        onCreate(db);
    }
}
