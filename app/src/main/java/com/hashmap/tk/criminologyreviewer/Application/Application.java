package com.hashmap.tk.criminologyreviewer.Application;

import com.hashmap.tk.criminologyreviewer.R;
import com.parse.Parse;
import com.parse.ParseACL;

public class Application extends android.app.Application {


    @Override
    public void onCreate() {
        super.onCreate();

        // Add your initialization code here

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getResources().getString(R.string.PARSE_APPLICATION_ID))
                .clientKey(getResources().getString(R.string.PARSE_CLIENT_KEY))
                .server("https://parseapi.back4app.com")
                .build());

//        ParseUser.enableRevocableSessionInBackground();
//        if (ParseUser.getCurrentUser()==null) {
//            ParseUser.enableAutomaticUser();
//            ParseUser.getCurrentUser().saveInBackground();
//        }

        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

//        ParseUser currentUser = ParseUser.getCurrentUser();
//        System.out.println("User : "+currentUser);
//        if (currentUser != null) {
//            startActivity(new Intent(getApplicationContext(),Activity_Main.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//        }else {
//            startActivity(new Intent(getApplicationContext(),Activity_Login.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//
//        }


    }
}
