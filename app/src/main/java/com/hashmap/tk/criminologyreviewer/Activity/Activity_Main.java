package com.hashmap.tk.criminologyreviewer.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hashmap.tk.criminologyreviewer.Database.Datasource;
import com.hashmap.tk.criminologyreviewer.Database.MySQLiteHelper;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_Admin;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_EditProfile;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_Exam;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_Help;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_Home;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_References;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_Result;
import com.hashmap.tk.criminologyreviewer.Fragment.Fragment_Settings;
import com.hashmap.tk.criminologyreviewer.Model.Category;
import com.hashmap.tk.criminologyreviewer.Model.Question;
import com.hashmap.tk.criminologyreviewer.R;
import com.hashmap.tk.criminologyreviewer.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Activity_Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static Fragment fragment;
    public static int page;
    public static TextView txtNavName, txtNavEmail;
    Toolbar toolbar;
    JSONObject jsonObject1, jsonObject2, jsonObject3, jsonObject4, jsonObject5;
    JSONArray jsonArray1, jsonArray2, jsonArray3, jsonArray4;
    JSONObject jsonFromAsset;
    String exams;
    Datasource datasource;
    MySQLiteHelper dbHelper;
    ProgressDialog dialog;
    ScrollView viewHome;
    ImageButton btnEdit;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private NavigationView navigationView;
    private RelativeLayout rel;
    private List<String> listDownload;
    private List<String> listReferences;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPref = new SharedPref(Activity_Main.this);
        fragment = new Fragment_Home();
        if (fragment != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        }

        datasource = new Datasource(Activity_Main.this);
        datasource.open();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (sharedPref.getKeyType().equals("user")) {
            navigationView.getMenu().getItem(1).setVisible(false);
        } else {
            navigationView.getMenu().getItem(1).setVisible(true);
        }
        View header = navigationView.getHeaderView(0);
        txtNavName = (TextView) header.findViewById(R.id.txtNavName);
        txtNavEmail = (TextView) header.findViewById(R.id.txtNavEmail);
        rel = (RelativeLayout) findViewById(R.id.container);
        btnEdit = (ImageButton) header.findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = null;
                fragment = new Fragment_EditProfile();
                toolbar.setTitle("Profile");
                if (fragment != null) {
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
//                     fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();

                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

//        ParseUser parseUser = ParseUser.getCurrentUser();
        try {
            if (sharedPref.getKeyObjectid()!=null) {
                txtNavName.setText(sharedPref.getKeyName());
//                txtNavEmail.setText(sharedPref.getKeyEmail());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<List<Category>, List<Question>> categoryList = null;
        dialog = new ProgressDialog(Activity_Main.this);

        if (!datasource.hasRows()) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    dialog.setIndeterminate(true);
                    dialog.setTitle("Initializing Database");
                    dialog.setMessage("This only occurs the first time this app runs");
                    dialog.setCancelable(false);
                    dialog.show();
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        jsonFromAsset = new JSONObject(loadJSONFromAsset());
                        exams = jsonFromAsset.getString("exams");
                        jsonObject1 = new JSONObject(exams);
                        jsonArray1 = jsonObject1.getJSONArray("category");
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            jsonObject2 = jsonArray1.getJSONObject(i);
                            //                        Log.i("Title", jsonObject2.getString("title"));
                            //                        Log.i("Set",jsonObject2.getString("text"));
                            jsonArray2 = jsonObject2.getJSONArray("question");

                            long id = datasource.addCategory(jsonObject2.getString("title"), jsonObject2.getString("text"));
                            for (int i2 = 0; i2 < jsonArray2.length(); i2++) {
                                jsonObject3 = jsonArray2.getJSONObject(i2);
                                //                            Log.i("|     Question",jsonObject3.getString("text"));
                                String question = jsonObject3.getString("text");
                                String answer = jsonObject3.getString("answer");
                                jsonArray3 = jsonObject3.getJSONArray("choice");
                                List<String> choicesList = new ArrayList<>();
                                for (int i3 = 0; i3 < jsonArray3.length(); i3++) {
                                    //                                Log.i("|       Choices",jsonArray3.get(i3).toString());
                                    choicesList.add(jsonArray3.get(i3).toString());
                                }
                                //                            Log.i("|     Answer",jsonObject3.getString("answer"));
                                datasource.addQuestion(id, question, choicesList, Integer.valueOf(answer));
                            }
                            //                        Log.i("Category",i+"-----------------------------------------------------");

                        }

                        String[] download = getResources().getStringArray(R.array.download_module);
                        String[] reference = getResources().getStringArray(R.array.category_module);
                        listDownload = Arrays.asList(download);
                        listReferences = Arrays.asList(reference);
                        if (!datasource.moduleHasRows()) {
                            datasource.addModule(listDownload);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    dialog.dismiss();
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }


    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("exams.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onBackPressed() {
//        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
//        Log.i("Backstack Count :",""+backStackEntryCount);
//        Log.i("Backstack Count :",""+getSupportFragmentManager().getBackStackEntryAt(backStackEntryCount-(1)).getName());
//        if(backStackEntryCount == 0){
//          viewHome.setVisibility(View.VISIBLE);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            try {
                fragmentTransaction.remove(fragment);
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onBackPressed();
//            exitApp();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity__main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            SharedPref sharedPref = new SharedPref(this);
//            Toast.makeText(Activity_Main.this, sharedPref.getTimeLimit() + "/" + sharedPref.getInitialization() + "/" + sharedPref.getSound() + "/" + sharedPref.getAutoNext(), Toast.LENGTH_SHORT).show();

            fragment = null;
//            rel.removeAllViews();
            fragment = new Fragment_Settings();

            if (fragment != null) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
//            fragmentTransaction.addToBackStack(fragment.getClass().getName());
//                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
//                viewHome.setVisibility(View.INVISIBLE);
//            Log.i("Backstack Count :", "" + fragment.getClass().getName());
                toolbar.setTitle(R.string.menu_settings);

            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        fragment = null;

        int id = item.getItemId();
//        if (id!=R.id.nav_logout||id!=R.id.nav_about) {
//            rel.removeAllViews();
//        }

        if (id == R.id.nav_home) {
            fragment = new Fragment_Home();
            toolbar.setTitle(R.string.menu_exam);
//            if (fragment != null) {
//                fragmentTransaction.remove(fragment);
////               int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
////               Log.i("Backstack Count :", "" + getSupportFragmentManager().getBackStackEntryAt(backStackEntryCount - (1)).getName());
//               fragment.onDetach();
//            }
//            rel.removeAllViews();
//            viewHome.setVisibility(View.VISIBLE);
//            toolbar.setTitle(R.string.menu_home);

        } else if (id == R.id.nav_admin) {
            fragment = new Fragment_Admin();
            toolbar.setTitle(R.string.menu_admin);
        } else if (id == R.id.nav_exams) {
            fragment = new Fragment_Exam();
            toolbar.setTitle(R.string.menu_exam);
        } else if (id == R.id.nav_references) {
            fragment = new Fragment_References();
            toolbar.setTitle(R.string.menu_references);
        } else if (id == R.id.nav_settings) {
            fragment = new Fragment_Settings();
            toolbar.setTitle(R.string.menu_settings);
        } else if (id == R.id.nav_result) {
            fragment = new Fragment_Result();
            toolbar.setTitle(R.string.menu_result);
        } else if (id == R.id.nav_help) {
            fragment = new Fragment_Help();
            toolbar.setTitle(R.string.menu_helpIns);
        } else if (id == R.id.nav_about) {
            showAbout();
        } else if (id == R.id.nav_logout) {
            logOut();
        }


        // Insert the fragment by replacing any existing fragment
        if (fragment != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            // fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();
//            viewHome.setVisibility(View.INVISIBLE);
//            Log.i("Backstack Count :", "" + fragment.getClass().getName());

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logOut() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Main.this);
        alertDialogBuilder.setTitle("Logout");
        alertDialogBuilder
                .setMessage("Are you sure?")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, int id) {
                        final ProgressDialog progressDialog = new ProgressDialog(Activity_Main.this);
                        progressDialog.setTitle("Log out");
                        progressDialog.setMessage("Logging out...");
                        progressDialog.setIndeterminate(true);
                        progressDialog.show();

                        try {
                            progressDialog.dismiss();
                            sharedPref.setKeyObjectid("");
                            sharedPref.setKeyUsername("");
                            sharedPref.setKeyPassword("");
                            sharedPref.setKeyEmail("");
                            sharedPref.setKeyName("");
                            sharedPref.setKeyType("");
                            Intent in = new Intent(getApplicationContext(), Activity_Login.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(in);
                            finish();
//                            ParseUser parseUser = ParseUser.getCurrentUser();
//                            if (parseUser.getUsername()!=null) {
//                                ParseUser.logOutInBackground(new LogOutCallback() {
//                                    @Override
//                                    public void done(ParseException e) {
//                                        if (e == null) {
//                                            sharedPref.setKeyObjectid("");
//                                            sharedPref.setKeyUsername("");
//                                            sharedPref.setKeyPassword("");
//                                            sharedPref.setKeyEmail("");
//                                            sharedPref.setKeyName("");
//                                            sharedPref.setKeyType("");
//                                            progressDialog.dismiss();
//                                            dialog.cancel();
//                                            Intent in = new Intent(getApplicationContext(), Activity_Login.class);
//                                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                            startActivity(in);
//                                            finish();
//                                        } else {
//                                            progressDialog.dismiss();
//                                            dialog.cancel();
//
//                                            Toast.makeText(Activity_Main.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
//                                        }
//                                    }
//                                });
//                            } else {
//                                progressDialog.dismiss();
//                                sharedPref.setKeyObjectid("");
//                                sharedPref.setKeyUsername("");
//                                sharedPref.setKeyPassword("");
//                                sharedPref.setKeyEmail("");
//                                sharedPref.setKeyName("");
//                                sharedPref.setKeyType("");
//                                Intent in = new Intent(getApplicationContext(), Activity_Login.class);
//                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(in);
//                                finish();
//                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void exitApp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Main.this);
        alertDialogBuilder.setTitle("Exit Application");
        alertDialogBuilder
                .setMessage("Do you really want to exit application ?")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void showAbout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Main.this);
        alertDialogBuilder.setTitle("About ");
        alertDialogBuilder
                .setMessage("PROGRAMMER:\n" +
                        "   Jerwin Zamora\n\n" +
                        "RESEARCHER:\n " +
                        "   Vanessa Alejandro\n" +
                        "    Apple Ressureccion\n\n" +
                        "CONSULTANT:\n" +
                        "   Prof. Desiree Cendana\n\n" +
                        "Approved by:\n" +
                        "   Mr. Joseph Mejia\n" +
                        "   Mr. Aristotle B. Liwanag")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onResume() {

        if (page != 0) {
            switch (page) {
                case 1:
                    fragment = new Fragment_Exam();
                    break;
                case 2:
                    fragment = new Fragment_References();
                    break;
                case 3:
                    fragment = new Fragment_Settings();
                    break;
                case 4:
                    fragment = new Fragment_Result();
                    break;

            }
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
            page = 0;
        }
        super.onResume();

    }

}
