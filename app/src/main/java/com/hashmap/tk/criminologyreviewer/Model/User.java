package com.hashmap.tk.criminologyreviewer.Model;

import java.util.ArrayList;

public class User {

    public static ArrayList<Student> studentList;
    public static boolean isNewAdded;

    public static String getUserObjId() {
        return userObjId;
    }

    public static void setUserObjId(String userObjId) {
        User.userObjId = userObjId;
    }

    public static String userObjId;

    public static ArrayList<Student> getStudentList() {
        return studentList;
    }

    public static void setStudentList(ArrayList<Student> studentList) {
        User.studentList = studentList;
    }

    public static boolean isNewAdded() {
        return isNewAdded;
    }

    public static void setIsNewAdded(boolean isNewAdded) {
        User.isNewAdded = isNewAdded;
    }
}
