package com.hashmap.tk.criminologyreviewer.Model;

public class Score {

    public static int answered = 0;
    public static int correct = 0;
    public static int wrong = 0;
    public String score;
    public String categoryName;
    public String categoryId;
    public String date;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public static int getAnswered() {
        return answered;
    }

    public static void setAnswered(int answered) {
        Score.answered = answered;
    }

    public static int getCorrect() {
        return correct;
    }

    public static void setCorrect(int correct) {
        Score.correct = correct;
    }

    public static int getWrong() {
        return wrong;
    }

    public static void setWrong(int wrong) {
        Score.wrong = wrong;
    }
}
